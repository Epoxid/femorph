#Functions for computing normals, curvature and similar geometric quantities

from dolfin import *
from SAD_MPI import GlobalizeFunction, SyncSum

import numpy

#orientate a mesh. Necessary if a shell mesh is read in.
def Orientate(mesh):
    #not parallel
    MaxDim = mesh.geometry().dim()
    if mpi_comm_world().Get_size()>1:
        print('Orienting not possible in parallel')
        exit()
    print('Orientation started...')

    def IsSame(c1, c2):
        c1V = numpy.zeros(MaxDim, dtype=int)
        c2V = numpy.zeros(MaxDim, dtype=int)
        i=0
        for v in vertices(c1):
            c1V[i] = v.index()
            i=i+1
        i=0
        for v in vertices(c2):
            c2V[i] = v.index()
            i = i+1

        c1f = [False, False, False]
        c2f = [False, False, False]

        for i in range(MaxDim):
            for j in range(MaxDim):
                if  c1V[i] == c2V[j]:
                    c1f[i] = True
                    c2f[j] = True
        for i in range(MaxDim):
            if c1f[i] == False:
                Extra1 = i
            if c2f[i] == False:
                Extra2 = i
        if (Extra1 == Extra2) or (Extra1 == 0 and Extra2 == 2) or (Extra1 == 2 and Extra2 == 0):
            return False
        else:
            return True

    NumCells = mesh.num_cells()
    mesh.init()
    mesh.init_cell_orientations(Expression(('1.0','1.0','1.0'), degree=1))
    tdim = mesh.topology().dim()
    print('    Computing CellNeighborhood...')
    cell_neighbors = {cell.index(): sum((filter(lambda ci: ci != cell.index(),
                                                facet.entities(tdim))
                                        for facet in facets(cell)), [])
                    for cell in cells(mesh)}
    oriented = numpy.zeros(mesh.num_cells(),dtype = 'int64')
    oriented[0] = 1
    old = 0
    defects = numpy.zeros(mesh.num_cells(),dtype = 'int64')
    defect = False
    while numpy.amin(oriented)==0:
        if old == int(numpy.linalg.norm(oriented, 1)):
            defect = True
            print('    surface not connected')
            for i in range(numpy.size(oriented)):
                if oriented[i]==0:
                    oriented[i] = 1
                    defects[i] = 1
                    break
        old = int(numpy.linalg.norm(oriented, 1))
        print('    NumOriented/NumCells: %d / %d'%(int(numpy.linalg.norm(oriented, 1)),NumCells))
        for i in range(numpy.size(oriented)):
            if oriented[i]==0:
                for j in range(len(cell_neighbors[i])):
                    if oriented[cell_neighbors[i][j]]==1:
                        c1 = Cell(mesh,i)
                        c2 = Cell(mesh,cell_neighbors[i][j])
                        same = IsSame(c1, c2)
                        if same:
                            mesh.cell_orientations()[i] = mesh.cell_orientations()[c2.index()]
                        else:
                            mesh.cell_orientations()[i] = -mesh.cell_orientations()[c2.index()]+1
                        oriented[i] = 1
                        if defect:
                            defects[i] = 1
                        break
    print('    NumOriented/NumCells: %d / %d'%(int(numpy.linalg.norm(oriented, 1)),NumCells))
    mesh.init()
    print('Mesh oriented')
    return mesh, defects

#returns a CG0 function containing the normal of a surface mesh only
#not intended for meshes with interior, use functions below instead
def BoundaryMeshNormal(gamma, SortNormal=True):
    if SortNormal and numpy.size(gamma.cell_orientations()) == 0:
        (gamma, defects) = Orientate(gamma)
    MaxDim = gamma.geometry().dim()
    VSpace = VectorFunctionSpace(gamma, "DG", 0, MaxDim)
    IntrinsicNormal = Function(VSpace)
    len = IntrinsicNormal.vector().local_size()
    values = numpy.zeros(len)
    dofs = FunctionSpace(gamma,"DG", 0).dofmap()
    for c in cells(gamma):
        k = c.index()
        DofID = dofs.cell_dofs(k)[0]
        Factor = -1.0
        if SortNormal:
            if gamma.cell_orientations() != []:
                Flip = gamma.cell_orientations()[k]
                if Flip  > 0.5:
                    Factor = 1.0
        for i in range(MaxDim):
            values[MaxDim*DofID+i] = Factor*c.cell_normal()[i]
    IntrinsicNormal.vector().set_local(values)
    IntrinsicNormal.vector().apply("")
    IntrinsicNormal.rename("normal", "")
    return IntrinsicNormal

#Average a per face vector field to a vertex CG1 vector field
#Input: FaceNormal, a DG0 function containing the normal.
def AverageFaceQuantity(FaceNormal, boundary_parts = [], markers = []):
    NSpace = FaceNormal.function_space()
    gamma = NSpace.mesh()
    MaxDim = NSpace.element().value_dimension(0)
    DofMapFace = NSpace.dofmap()
    #local storage for three vector components per vertex of the whole mesh
    LocValues = numpy.zeros(len(FaceNormal.vector()))
    LocNumAverages = numpy.zeros(len(FaceNormal.vector())/MaxDim)
    GlobalFaceNormal = GlobalizeFunction(FaceNormal)
    for v in vertices(gamma):
        Index = v.index()
        #PARALLEL: Turn local mesh index into global mesh index
        #IndexGlobal = GT.global_indices(0)[Index]
        IndexGlobal = v.global_index()
        Faces = cells(v)#v.entities(MaxDim-1)
        FNormalSum = numpy.zeros(MaxDim)
        MyNumFaces = 0.0
        for f in Faces:
            i = f.index()
            GID = f.global_index()
            if markers != []:
                FaceMarker = boundary_parts.array()[i]
                IsInMarker = FaceMarker in markers
            else:
                IsInMarker = True
            if IsInMarker:
                MyNumFaces = MyNumFaces + 1.0
                FaceDofs = DofMapFace.cell_dofs(i)#Local-to-global mapping of dofs on a cell
                for j in range(MaxDim):
                    ID1 = DofMapFace.local_to_global_index((FaceDofs[j]))
                    value = GlobalFaceNormal[ID1]#FaceNormal.vector()[ID1]
                    FNormalSum[j] = FNormalSum[j] + value
        #LocNumAverages[IndexGlobal] = len(Faces)
        LocNumAverages[IndexGlobal] = MyNumFaces
        for j in range(MaxDim):
            LocValues[MaxDim*IndexGlobal + j] = FNormalSum[j]
    LocValues = SyncSum(LocValues)
    LocNumAverages = SyncSum(LocNumAverages)
    #from now on, LocValues actually contains the globally summed face normals into vertex normals
    MoveSpaceS = FunctionSpace(gamma, "CG", 1)
    MoveSpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
    VertexNormal = Function(MoveSpaceV)
    dof_min, dof_max = VertexNormal.vector().local_range()
    dof_max = int(dof_max/MaxDim)
    dof_min = int(dof_min/MaxDim)
    ll = dof_max - dof_min
    MyValues = numpy.empty(MaxDim*ll)
    for i in range(ll):
        GDof = i#dof_min + i
        vert = dof_to_vertex_map(MoveSpaceS)[GDof]
        MyVert = MeshEntity(gamma, 0, vert)
        IndexGlobal = MyVert.global_index()
        #IndexGlobal = GT.global_indices(0)[vert]
        quotient = LocNumAverages[IndexGlobal]
        if quotient > DOLFIN_EPS:
            norm = 0.0
            for j in range(MaxDim):
                NormalJ = LocValues[MaxDim*IndexGlobal + j]/quotient
                MyValues[MaxDim*i+j] = NormalJ
                norm = norm + NormalJ*NormalJ
            norm = sqrt(norm)
            for j in range(MaxDim):
                MyValues[MaxDim*i+j] = MyValues[MaxDim*i+j]/norm
    VertexNormal.vector().set_local(MyValues)
    VertexNormal.vector().apply('')
    VertexNormal.rename(FaceNormal.name(), "dummy")
    return VertexNormal

#return a CG-1 function in the volume that is the vertex normal on the boundary and zero in the volume
#Symm is an array containing 0,1 or 2 to indicate the symmetry plane
def VolumeNormal(mesh, Symm = [], boundary_parts = None, InteriorIDs = None):
    mesh.init()
    dim = mesh.topology().dim()
    NumVertex = mesh.num_entities_global(0)
    MyNormal = numpy.zeros([NumVertex, dim])
    NumAvg = numpy.zeros(NumVertex)
    if InteriorIDs == None:
        for f in facets(mesh):
            if f.exterior() and f.is_ghost() == False:
                for v in vertices(f):
                    vID = v.global_index()
                    for j in range(dim):
                        MyNormal[vID, j] += f.normal(j)
                    NumAvg[vID] += 1
    else:
        ct = mesh.type()
        for c in cells(mesh):
            fcount = 0
            for f in facets(c):
                if f.is_ghost() == False and (f.exterior() or boundary_parts[f.index()] in InteriorIDs):
                    fnormal = ct.normal(c, fcount)
                    for v in vertices(f):
                        vID = v.global_index()
                        for j in range(dim):
                            MyNormal[vID, j] += f.normal(j)
                        NumAvg[vID] += 1
                fcount += 1
    SyncSum(MyNormal)
    SyncSum(NumAvg)
    SSpace = FunctionSpace(mesh, "CG", 1)
    V2D = vertex_to_dof_map(SSpace)
    N = Function(VectorFunctionSpace(mesh, "CG", 1, dim))
    LocValues = numpy.zeros(N.vector().local_size())
    OwnerRange = SSpace.dofmap().ownership_range()
    for v in vertices(mesh):
        vID = v.index()
        vIDG = v.global_index()
        DDof = V2D[v.index()]
        IsOwned = OwnerRange[0] <= SSpace.dofmap().local_to_global_index(DDof) and SSpace.dofmap().local_to_global_index(DDof) < OwnerRange[1]
        if IsOwned and NumAvg[vIDG] > 0.0:
            for j in range(dim):
                LocValues[dim*DDof+j] = MyNormal[vIDG, j]/NumAvg[vIDG]
            #Apply Symmetry Plane
            for i in Symm:
                if near(v.x(i), 0.0, DOLFIN_EPS):
                    LocValues[dim*DDof+i] = 0.0
            #Normalize
            MyValue = 0.0
            for j in range(dim):
                MyValue += LocValues[dim*DDof+j]*LocValues[dim*DDof+j]
            MyValue = sqrt(MyValue)
            if MyValue > 1e-6:
                for j in range(dim):
                    LocValues[dim*DDof+j] /= MyValue
            else:
                for j in range(dim):
                    LocValues[dim*DDof+j] = 0.0
    N.vector().set_local(LocValues)
    N.vector().apply("")
    N.rename("SAD_VOLUME_NORMAL", "dummy")
    return N

#Computes kappa by kappa*n = SurfLaplace X, with X surface point coordinates
#Returns a boundary function, which cannot be used in *ds integrals of surface functions
#Most of the time it's better to use ComputeDivNVolume Function below instead
#Check if factor 2.0 is missing!
def ComputeMeanCurvature(mesh, gamma, SmoothEps = 0.0):
    deg = 1
    MaxDim = gamma.topology().dim()+1
    SmoothEps = Constant(SmoothEps)
    S = FunctionSpace(gamma, "CG", deg)
    V = VectorFunctionSpace(gamma, "CG", deg, MaxDim)
    #V = FunctionSpace(gamma, "CG", deg)
    if MaxDim == 1:
        X = interpolate(Expression("x[0]", degree=ExprDegree), V)
        Z = Constant(0.0)
    elif MaxDim == 2:
        X = interpolate(Expression(("x[0]", "x[1]"), degree=ExprDegree), V)
        Z = Constant((0.0, 0.0))
    elif MaxDim == 3:
        X = interpolate(Expression(("x[0]", "x[1]", "x[2]"), degree=ExprDegree), V)
        Z = Constant((0.0, 0.0, 0.0))
    #make point normal
    #Face normal
    N = SurfaceFaceNormal(mesh)
    ##point normal
    N = AverageFaceQuantity(N)
    NormalizeVectorField(N)
    h = TrialFunction(V)
    v = TestFunction(V)
    a = (SmoothEps*inner(grad(v), grad(h)) + Constant(2.0)*inner(h,v))*dx
    l = inner(grad(v), grad(X))*dx
    HN = Function(V)
    #Enforce Dirichlet Zero on open boundaries...
    bc0 = DirichletBC(V, Z, "on_boundary")
    solve(a==l, HN, bc0)
    #compute norm <--- WRONG! Removes SIGN!
    #multiply with transpose(n) <--- RIGHT!
    H = Function(S)
    (dmin, dmax) = H.vector().local_range()
    LocData = numpy.zeros(dmax-dmin)
    for i in range(0, dmax-dmin):
        value = 0.0
        for j in range(MaxDim):
            data1 = HN.vector()[MaxDim*(i+dmin)+j]
            data2 = N.vector()[MaxDim*(i+dmin)+j]
            value += data1*data2
        #value = sqrt(float(value))
        LocData[i] = value
    H.vector().set_local(LocData)
    H.vector().apply("")
    H.rename("curvature", "dummy")
    return H

#compute the mean curvature by calculating tangent_div n
#Returns a boundary function, which cannot be used in *ds integrals of surface functions
#Most of the time it's better to use ComputeDivNVolume Function below instead
def ComputeDivN(mesh, SmoothEps = 0.0, boundary_parts = [], markers = []):
    mesh.init()
    gamma = BoundaryMesh(mesh, "exterior")
    gamma.init()
    if markers != []:
        bp = ReduceMeshFunctionToSurface(mesh, boundary_parts)
    else:
        bp = []
        
    N = SurfaceFaceNormal(mesh)
    N = AverageFaceQuantity(N, bp, markers)
    NormalizeVectorField(N)
        
    if markers != []:
        (gamma_sub, dummy) = ExtractSubsurface_old(markers, mesh, boundary_parts)
        N = ReduceVectorFunctionToSubsurface(N, mesh, gamma_sub, "CG", 1)
    else:
        gamma_sub = gamma
    N = project(N, VectorFunctionSpace(gamma_sub, "CG", 1, 3))
        
    S = FunctionSpace(gamma_sub, "CG", 1)
    u = TrialFunction(S)
    v = TestFunction(S)
    a = Constant(1.0)*dot(v,u)*dx + Constant(SmoothEps)*inner(grad(v), grad(u))*dx
    l = inner(v, Constant(0.0))*dx + inner(v, div(N))*dx
    kappa = Function(S)
    solve(a==l, kappa)
        
    if markers != []:
        kappa = ExpandFunctionFromSubsurface(kappa, gamma, gamma_sub, "CG", 1)
    kappa.rename("Curvature", "dummy")
    return kappa

def ComputeDivNVolume(mesh, SmoothEps, N = None, boundary_parts=None, InteriorEdges=None):
    if N == None:
        N = VolumeNormal(mesh, [])
    Space = FunctionSpace(mesh, "CG", 1)
    w = TrialFunction(Space)
    v = TestFunction(Space)
    l = v*(div(N)-dot(grad(N)*N,N))*ds
    a = inner(v,w)*dx
    a += (inner(v,w) + SmoothEps*inner(grad(v)-dot(grad(v),N)*N, grad(w)-dot(grad(w),N)*N))*ds
    if boundary_parts != None:
        dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
        for i in InteriorEdges:
            l += avg((v)*((div(N))-dot((grad(N))*N,N)))*dS(i)
            a += avg(inner((v),(w)))*dS(i)
    kappa = Function(Space)
    solve(a==l, kappa)
    kappa.rename("Curvature", "dummy")
    return kappa

def ComputeVolume(mesh):
    dxx = dx(domain=mesh)
    vol = assemble(Constant(1.0)*dxx)
    return vol

def ComputeBarycenter(mesh, subdomain=None):
    MaxDim = mesh.topology().dim()
    dxx = dx(domain=mesh)
    Barycenter = MaxDim*[0.0]
    vol = ComputeVolume(mesh)
    for i in range(MaxDim):
        MyBC = 1.0/vol*assemble(Expression("x[%d]"%i, degree=2)*dxx(subdomain))
        Barycenter[i] = MyBC
    return Barycenter

def SD_Bary(mesh, V, N):
    MaxDim = mesh.topology().dim()
    #dss = ds(domain=mesh)
    vol = ComputeVolume(mesh)
    Bary = ComputeBarycenter(mesh)
    SDB = MaxDim*[0.0]
    for k in range(MaxDim):
        MyXi = Expression("x[%d]"%k, degree=2)
        MyXi.rename("x_%d"%k, "")
        volb1 = Constant(1.0/vol)
        volb1.rename("1/vol", "")
        Baryk = Constant(Bary[k])
        Baryk.rename("Bary_%d"%k, "")
        SDB[k] = volb1*dot(V,N)*(MyXi - Baryk)
    return SDB

### V1 Routines ###

def CalculateArea(mesh, Marker=[], boundary_parts=[]):
    Area = 0.0
    temp = project(Constant(1.0), FunctionSpace(mesh, 'CG', 1))
    if Marker == []:
        Area = assemble(temp*ds)
    else:
        dss = ds(domain=mesh, subdomain_data=boundary_parts)
        for i in Marker:
            Area += assemble(temp*dss(i))
    return Area
    
def CalculateVolume(mesh):
    Volume = project(Constant(1.0), FunctionSpace(mesh, "CG", 1))*dx
    return assemble(Volume)
    
def CalculateCentroid(mesh, Marker):
    MaxDim = mesh.topology().dim()
    dss = Measure("ds")[boundary_parts]
    Vol = CalculateArea(mesh, VariableBoundary)
    Space = FunctionSpace(mesh, "CG", 1)
    X = []
    for j in range(MaxDim):
        x = project(Expression("x[j]", j=j, degree=ExprDegree), Space)
        c = (1.0/Vol)*assemble(x*dss(Marker))
        X.append(c)
    return X
    
def BisectVolume(mesh, TargetVolume, TargetCentroid = [], InitField = [], Quadrants = [], tol = 2e-3, MinDist = 1.25e-2):
    gamma = BoundaryMesh(mesh, "exterior")
    N = MakeV(mesh, gamma)
    ll = 0.0
    lr = 0.5
    eps = 1e-5
    alpha = 0.0
    SpaceV = VectorFunctionSpace(gamma, "CG", 1)
    while lr - ll > eps:
        alpha = 0.5*(ll+lr)
        MeshCopy = Mesh(mesh)
        GammaCopy = BoundaryMesh(MeshCopy, "exterior")
        if InitField != []:
            Field = project(InitField + Constant(-alpha)*N, SpaceV)
        else:
            Field = project(Constant(-alpha)*N, SpaceV)
        if Quadrants != []:
            Field = project(Field + FixQuadrant(gamma, mesh.topology().dim(), Quadrants, tol, MinDist, InitField), SpaceV)
        GammaCopy.move(Field)
        MeshCopy.move(GammaCopy)
        Vol = CalculateVolume(MeshCopy)
        if Vol > TargetVolume: ll = alpha
        elif Vol < TargetVolume: lr = alpha
    if TargetCentroid != []:
        MaxDim = mesh.topology().dim()
        Centroid = CalculateCentroid(mesh, VariableBoundary)
        CDiff = [0.0]*MaxDim
        for j in range(MaxDim):
            CDiff[j] = Centroid[j] - TargetCentroid[j]
        if MaxDim == 2:
            CDiff = project(Expression(("a", "b"), a = CDiff[0], b=CDiff[1], element=SpaceV.ufl_element()), SpaceV)
        if MaxDim == 3:
            CDiff = project(Expression(("a", "b", "c"), a = CDiff[0], b=CDiff[1], c=CDiff[2], element=SpaceV.ufl_element()), SpaceV)
        CDiff = InjectOnSubSurface(CDiff, mesh, VariableBoundary)
        Field = project(Field - CDiff, SpaceV)
    return Field
