********************
Installing femorph
********************

Docker
==========

Docker is the easiest way of running this software if you do not have
FEniCS installed locally on your computer. Installation instructions for docker can be found at `Docker
<https://www.docker.com/community-edition>`_.

We then initialize a docker container with a FEniCS-image::

     docker run -ti -v $(pwd):/home/fenics/shared quay.io/fenicsproject/stable:2017.2.0.r4

Then, we install gmsh in the docker container::

     /bin/sh -c "sudo apt-get update; sudo apt-get install -y libgl1-mesa-glx libxcursor1 libxft2 libxinerama1 libglu1-mesa"
     wget -nc --quiet gmsh.info/bin/Linux/gmsh-2.12.0-Linux64.tgz
     tar -xf gmsh-2.12.0-Linux64.tgz
     sudo cp -r gmsh-2.12.0-Linux/share/* /usr/local/share/
     sudo cp gmsh-2.12.0-Linux/bin/* /usr/local/bin
