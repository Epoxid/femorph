#Shape Optimization and Geometry Toolbox for FEniCS
#by Stephan Schmidt, 2016

from dolfin import *

import SAD_MPI

from SAD_tangent_differentiation import *

import numpy
import os
import math

#Modes:
Strong = 0 #strong form, boundary expressions
WeakLocal = 1 #weak form with local derivatives, aka "div(f*V) + f'"
WeakMaterial = 2 #weak form with material derivatives, aka "div(V)*f + df"

ExprDegree = 2 #base degree for all expressions

def ShapeDerivative(form, mesh, V, n=None, State=None, StateDirection=None, kappa=None, Is_Normal=[], Constant_In_Normal=[], boundary_parts = None, SymmetryDirection = None, IncludeNormalVariation = True, IncludeCurvatureVariation=True, Mode=Strong):
    
    from ufl import algorithms
    from ufl import differentiation
    from ufl import checks

    from ufl.algorithms.map_integrands import map_integrand_dags

    from SAD_apply_derivatives import SAD_apply_derivatives

    import SAD_simplify
    from SAD_derivatives import SAD_material_derivative
    
    mdx = dx(domain=mesh)
    if boundary_parts != None:
        mds = ds(domain=mesh, subdomain_data=boundary_parts)
    else:
        mds = ds(domain=mesh)

    from ufl import algorithms

    MyTerms = []
    for i in form.integrals():
        MySubDomain = i.subdomain_id()
        MyMeasure = i.integral_type()
        MyIntegrand = i.integrand()
        #immediatly throw out all constants, extra knowledge, etc...
        MyIntegrand = SAD_simplify.CreateStandardExpression(MyIntegrand)
        if kappa != None and n != None:
            MyIntegrand = SAD_simplify.ApplySimplifications(MyIntegrand, n, kappa, Is_Normal, Constant_In_Normal)
            if SymmetryDirection != None:
                MyIntegrand = SAD_simplify.IncorporateSymmetry(MyIntegrand, SymmetryDirection, V, n)
    
        if MyMeasure == "cell":
            MyIntegrand = SAD_simplify.CreateStandardExpression(MyIntegrand)
            #Volume / Weak Shape Derivative
            if Mode == WeakMaterial:
                Expr = div(V)*MyIntegrand
                if State != None:
                    """
                    if isinstance(State, (list, tuple)):
                        for scounter in range(len(State)):
                            Material = derivative(MyIntegrand, State[scounter], StateDirection[scounter])
                            Material = SAD_material_derivative(Material, V)
                            Expr = Expr + Material
                    else:
                    """
                    if True:
                        Material = derivative(MyIntegrand, State, StateDirection)
                        Material = SAD_material_derivative(Material, V)
                        Expr = Expr + Material
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                SAD_simplify.MySumAppend(Expr, mdx(MySubDomain), MyTerms)

            elif Mode == WeakLocal:
                Expr = div(MyIntegrand*V)
                if State != None:
                    """
                    if isinstance(State, (list, tuple)):
                        for scounter in range(len(State)):
                            Local = derivative(MyIntegrand, State[scounter], StateDirection[scounter])
                            Local = SAD_simplify.CreateStandardExpression(Local)
                            Expr = Expr + Local
                    else:
                    """
                    if True:
                        Local = derivative(MyIntegrand, State, StateDirection)
                        Local = SAD_simplify.CreateStandardExpression(Local)
                        Expr = Expr + Local
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                SAD_simplify.MySumAppend(Expr, mdx(MySubDomain), MyTerms)

            #Boundary / Strong Shape Derivative
            elif Mode == Strong:
                Expr = inner(V, n)*MyIntegrand
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                SAD_simplify.MySumAppend(Expr, mds, MyTerms)
                if State != None:
                    from SAD_derivatives import MyIsZero
                    """
                    if isinstance(State, (list, tuple)):
                        for scounter in range(len(State)):
                            if MyIsZero(StateDirection[scounter]) == False:
                                Local = derivative(MyIntegrand, State[scounter], StateDirection[scounter])
                                Local = SAD_simplify.CreateStandardExpression(Local)
                                SAD_simplify.MySumAppend(Local, mdx(MySubDomain), MyTerms)
                    else:
                    """
                    if True:
                        if MyIsZero(StateDirection) == False:
                            Local = derivative(MyIntegrand, State, StateDirection)
                            Local = SAD_simplify.CreateStandardExpression(Local)
                            SAD_simplify.MySumAppend(Local, mdx(MySubDomain), MyTerms)
            else:
                print("Shape Derivative: Error: Unrecognized mode")
                exit(1)
        #processing facet integral (strong and weak form):
        elif MyMeasure == "exterior_facet" or MyMeasure == "interior_facet":
            if boundary_parts != None:
                if MyMeasure == "exterior_facet":
                    mds = ds(domain=mesh, subdomain_data=boundary_parts)
                else:
                    mds = dS(domain=mesh, subdomain_data=boundary_parts)
            else:
                if MyMeasure == "exterior_facet":
                    mds = ds(domain=mesh)
                else:
                    mds = dS(domain=mesh)

            if "grad(n)" in str(MyIntegrand) or "div(n)" in str(MyIntegrand):
                print("Shape Derivative: Differential operators on normal not supported, sorry...")
                exit()
            #Volume / Weak Shape Derivative
            if Mode == WeakMaterial:
                if V not in Constant_In_Normal:
                    if n == None:
                        print("Need to provide a normal to compute tangential divergence")
                        exit(1)
                    Expr = div_tan(V, n)*MyIntegrand
                else:
                    Expr = div(V)*MyIntegrand
            
                if State != None:
                    """
                    if isinstance(State, (list, tuple)):
                        for scounter in range(len(State)):
                            Material = derivative(MyIntegrand, State[scounter], StateDirection[scounter])
                            Material = SAD_material_derivative(Material, V)
                            Expr = Expr + Material
                    else:
                    """
                    if True:
                        Material = derivative(MyIntegrand, State, StateDirection)
                        Material = SAD_material_derivative(Material, V)
                        Expr = Expr + Material

                Expr = SAD_simplify.CreateStandardExpression(Expr)
                SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
                #Test for dependency on normal:
                if n != None:
                    dn = Function(VectorFunctionSpace(mesh, "CG", 1))
                    dn.rename("SAD_dn", "")
                    NV0 = derivative(MyIntegrand, n, dn)
                    NV0 = SAD_simplify.CreateStandardExpression(NV0)
                    if NV0 != 0 and IncludeNormalVariation:
                        D_tanV = grad_tan(V, n)
                        dn2 = dot(n,D_tanV) #same as -dot(transpose(D_tanV),n)
                        #do sign later...
                        NormalExpr = derivative(MyIntegrand, n, dn2)
                        NormalExpr = SAD_simplify.CreateStandardExpression(-NormalExpr)
                        SAD_simplify.MySumAppend(NormalExpr, mds(MySubDomain), MyTerms)
            #Surface/strong form
            else:
                if kappa == None:
                    print("Shape Derivative: Boundary Integrals Require Curvature. Please specify")
                    exit()
                #use the well-known formula (V,n)*(dg/dn + kappa*g)
                #compute partial spatial derivative without normal (psd)
                isconstant = checks.is_globally_constant(MyIntegrand)
                if Mode == WeakLocal:
                    psd1 = div_tan(V, n)*MyIntegrand
                    psd1 = SAD_simplify.CreateStandardExpression(psd1)
                    psd1 = SAD_simplify.ApplySimplifications(psd1, n, kappa, Is_Normal, Constant_In_Normal)
                    if psd1 != as_ufl(0.0):
                        if SymmetryDirection != None:
                            psd1 = SAD_simplify.IncorporateSymmetry(psd1, SymmetryDirection, V, n)
                        if MyMeasure == "interior_facet":
                            psd1 = 2.0*avg(psd1)
                        SAD_simplify.MySumAppend(psd1, mds(MySubDomain), MyTerms)
                elif Mode == Strong and not isconstant:
                    psd1 = inner(V, n)*inner(grad(MyIntegrand), n)
                    psd1 = SAD_simplify.CreateStandardExpression(psd1)
                    psd1 = SAD_simplify.ApplySimplifications(psd1, n, kappa, Is_Normal, Constant_In_Normal)
                    if psd1 != as_ufl(0.0):
                        if SymmetryDirection != None:
                            psd1 = SAD_simplify.IncorporateSymmetry(psd1, SymmetryDirection, V, n)
                        if MyMeasure == "interior_facet":
                            psd1 = 2.0*avg(psd1)
                        SAD_simplify.MySumAppend(psd1, mds(MySubDomain), MyTerms)
                if Mode == WeakLocal and not isconstant:
                    psd2 = inner(grad(MyIntegrand),V)
                    psd2 = SAD_simplify.CreateStandardExpression(psd2)
                    psd2 = SAD_simplify.ApplySimplifications(psd2, n, kappa, Is_Normal, Constant_In_Normal)
                    if SymmetryDirection != None:
                        psd2 = SAD_simplify.IncorporateSymmetry(psd2, SymmetryDirection, V, n)
                    if MyMeasure == "interior_facet":
                        psd2 = 2.0*avg(psd2)
                    SAD_simplify.MySumAppend(psd2, mds(MySubDomain), MyTerms)
                elif Mode == Strong:
                    psd2 = inner(V, n)*kappa*MyIntegrand
                    psd2 = SAD_simplify.CreateStandardExpression(psd2)
                    psd2 = SAD_simplify.ApplySimplifications(psd2, n, kappa, Is_Normal, Constant_In_Normal)
                    if SymmetryDirection != None:
                        psd2 = SAD_simplify.IncorporateSymmetry(psd2, SymmetryDirection, V, n)
                    if MyMeasure == "interior_facet":
                        psd2 = 2.0*avg(psd2)
                    SAD_simplify.MySumAppend(psd2, mds(MySubDomain), MyTerms)
                #Test for dependency on normal:
                if n != None:
                    dn = Function(VectorFunctionSpace(mesh, "CG", 1))
                    dn.rename("SAD_dn", "")
                    NV0 = derivative(MyIntegrand, n, dn)
                    NV0 = SAD_simplify.CreateStandardExpression(NV0)
                    if NV0 != 0 and IncludeNormalVariation:
                        if not V in Is_Normal:
                            D_tanV = grad(V) - outer(dot(grad(V),n),n)
                            dn2 = dot(n,D_tanV) #same as -dot(transpose(D_tanV),n)
                            #do sign later...
                            Expr = derivative(MyIntegrand, n, dn2)
                            Expr = SAD_simplify.CreateStandardExpression(-Expr)
                            SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
                        else:
                            NewExpr = SAD_simplify.ApplyTangentStokes(NV0, dn, V, n, kappa, Is_Normal+[n])
                            if NewExpr != as_ufl(0.0):
                                NewExpr = SAD_simplify.CreateStandardExpression(NewExpr)
                                NewExpr = SAD_simplify.ApplySimplifications(NewExpr, n, kappa, Is_Normal, Constant_In_Normal)
                                if SymmetryDirection != None:
                                    NewExpr = SAD_simplify.IncorporateSymmetry(NewExpr, SymmetryDirection, V, n)
                                SAD_simplify.MySumAppend(NewExpr, mds(MySubDomain), MyTerms)
                #Add local derivative
                if State != None:
                    Local = derivative(MyIntegrand, State, StateDirection)
                    Local = SAD_simplify.CreateStandardExpression(Local)
                    SAD_simplify.MySumAppend(Local, mds(MySubDomain), MyTerms)
            #Test for curvature dependency:
            if kappa != None:
                NK = differentiation.VariableDerivative(MyIntegrand,kappa)
                NK = SAD_simplify.CreateStandardExpression(NK)
                if NK != 0 and IncludeCurvatureVariation:
                    if NK == as_ufl(1.0):
                        NK = Expression("1.0", domain=mesh, degree=ExprDegree)
                        NK.rename("1.0", "")
                    if V in Is_Normal:
                        OperatorTest = SAD_simplify.TestOperator()
                        beta = inner(SymmetryDirection, n)
                        def TreatCurvature(o):
                            MyType = OperatorTest(o)
                            if MyType == "SAD_SUM":
                                a, b = o.ufl_operands
                                Expr1 = TreatCurvature(a)
                                Expr2 = TreatCurvature(b)
                                return Expr1 + Expr2
                            CList = []
                            SAD_simplify.CollectProduct(o, CList)
                            if beta in CList:
                                CList.remove(beta)
                                if beta in CList:
                                    print("Shape Derivative: Curvature: Error: inner(V, n) not linear!")
                                    exit(1)
                                f = CList[0]
                                for i in range(1, len(CList)):
                                    f = f*CList[i]
                                Expr = f*inner(grad_tan(beta, n), grad_tan(inner(V, n), n))
                                #Expr = Expr + inner(V, n)*beta*div_tan(grad_tan(f, n),n)
                                Expr = Expr + inner(V, n)*beta*laplace_tan(f, n, kappa)
                                #Expr = Expr - inner(grad_tan(inner(V, n)*beta, n), grad_tan(f, n))
                                return Expr
                            else:
                                #1x integration by parts
                                Expr = inner(grad(NK), dot(n, grad(V))) - inner(grad(NK),n)*inner(n, dot(n, grad(V)))
                                #2x integration by parts
                                #Expr = inner(V, n)*(div_tan(grad_tan(NK, n), n))
                            return Expr
                        Expr = TreatCurvature(NK)
                    else:
                        Expr = inner(grad_tan(NK, n), grad_tan(inner(V, n), n))
                    if Expr != None:
                        Expr = SAD_simplify.CreateStandardExpression(Expr)
                        Expr = SAD_simplify.ApplySimplifications(Expr, n, kappa, Is_Normal, Constant_In_Normal)
                        if SymmetryDirection != None:
                            Expr = SAD_simplify.IncorporateSymmetry(Expr, SymmetryDirection, V, n)
                    SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
        #neither surface nor volume integral
        else:
            print("ShapeDerivative: Unknown integral type")
            exit(1)
            #raise ValueError, "Unknown integral type: {}.".format(MyMeasure)
    #create final expression
    ShapeDerivative = SAD_simplify.MyBuildSADForm(MyTerms, SymmetryDirection, V, n)
    return ShapeDerivative
