//obstacle data
cl_inner = 0.1;
a = 0.5;
b = 0.5;

//channel bounding box
cl_outer = 2.0;
x1 = -1.6;
y1 = -1.5;
length = 9.0;

//boundary markers
Outlet = 2;
Inlet = 3;
Design = 4;
Noslip = 5;

Point(1) = {0.0, 0.0, 0.0, cl_inner};
Point(2) = {a, 0.0, 0.0, cl_inner};
Point(3) = {0.0, b, 0.0, cl_inner};
Point(4) = {-a, 0.0, 0.0, cl_inner};
Point(5) = {0.0, -b, 0.0, cl_inner};
Ellipse(1) = {2, 1, 1, 3};
Ellipse(2) = {3, 1, 1, 4};
Ellipse(3) = {4, 1, 1, 5};
Ellipse(4) = {5, 1, 1, 2};

//bounding box
Point(6) = {x1, y1, 0.0, cl_outer};
Point(7) = {x1, -y1, 0.0, cl_outer};
Point(8) = {x1+length, y1, 0.0, cl_outer};
Point(10) = {x1+length, -y1, 0.0, cl_outer};
//extra points for local refinement
Point(11) = {x1, 0.0, 0.0, cl_outer};
Point(12) = {x1+length, 0.0, 0.0, cl_outer};
Line(5) = {7, 10};
Line(6) = {10, 12};
Line(7) = {12, 8};
Line(8) = {8, 6};
Line(9) = {6, 11};
Line(10) = {11, 7};
//Trennlinie
Line(12) = {11, 4};
Line(13) = {2, 12};
//Define Surfaces
Line Loop(1) = {10, 5, 6, -13, 1, 2, -12};
Plane Surface(1) = {1};
Line Loop(2) = {9, 12, 3, 4, 13, 7, 8};
Plane Surface(2) = {2};

//Nachlauf
Field[1] = MathEval;
Field[1].F = "2.0*(0.05*(abs(x-0.7) + abs(y) + abs(z))+0.01)";
//BoundaryLayer
Field[2] = MathEval;
Field[2].F = "2.0*(0.05*(x*x+y*y+z*z)+0.001)";
//Channel Boundary
Field[3] = MathEval;
Field[3].F = "2.0*(0.2*(y-1.5)^1.6+0.1)";
Field[4] = MathEval;
Field[4].F = "2.0*(0.2*(y+1.5)^1.6+0.1)";
//Outlet
Field[5] = MathEval;
Field[5].F = "2.0*(0.8*(x-7.4)^1.5+0.1)";
Field[6] = Min;
Field[6].FieldsList = {1,2,3,4,5};

//Sanity
//Field[7] = MathEval;
//Field[7].F = "0.08";
//Field[8] = Max;
//Field[8].FieldsList = {6,7};
//Field[1].F = "Cos(4*3.14*x) * Sin(4*3.14*y) / 10 + 0.101";
Background Field = 6;
Physical Line(Inlet) = {9,10};
Physical Line(Outlet) = {6,7};
Physical Line(Design) = {2, 1, 4, 3};
Physical Line(Noslip) = {5, 8};
Physical Surface(0) = {1,2};
