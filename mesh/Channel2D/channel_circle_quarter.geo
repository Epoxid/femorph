//obstacle data
cl_inner = 0.1;
a = 0.5;
b = 0.5;

//channel bounding box
cl_outer = 2.0;
x1 = -1.7;
y1 = 1.5;
length = 9.0;

//boundary markers
Outlet = 2;
Inlet = 3;
Design = 4;
Noslip = 5;
Symm = 6;

Point(1) = {0.0, 0.0, 0.0, cl_inner};
Point(2) = {a, 0.0, 0.0, cl_inner};
Point(3) = {0.0, b, 0.0, cl_inner};
Point(4) = {-a, 0.0, 0.0, cl_inner};
Ellipse(1) = {2, 1, 1, 3};
Ellipse(2) = {3, 1, 1, 4};

//bounding box
Point(6) = {x1, y1, 0.0, cl_outer};
Point(7) = {x1, 0.0, 0.0, cl_outer};
Point(8) = {x1+length, y1, 0.0, cl_outer};
Point(10) = {x1+length, 0.0, 0.0, cl_outer};
//extra points for local refinement
Point(11) = {x1, 0.0, 0.0, cl_outer};
Point(12) = {x1+length, 0.0, 0.0, cl_outer};

//create edges
Line(3) = {7, 6};
Line(4) = {6, 8};
Line(5) = {8, 10};
Line(6) = {10, 2};
Line(7) = {4, 7};
Line Loop(8) = {3, 4, 5, 6, 1, 2, 7};
Plane Surface(9) = {8};

//Nachlauf
Field[1] = MathEval;
Field[1].F = "2.0*(0.02*(abs(x-0.8) + abs(y) + abs(z)))";
Field[2] = MathEval;
Field[2].F = "0.05";
Field[3] = Max;
Field[3].FieldsList = {1,2};
//BoundaryLayer
Field[4] = MathEval;
Field[4].F = "2.0*(0.05*(x*x+y*y+z*z)+0.001)";
//Channel Boundary
Field[5] = MathEval;
Field[5].F = "2.0*(0.2*(y-1.5)^1.6+0.1)";
Field[6] = MathEval;
Field[6].F = "2.0*(0.2*(y+1.5)^1.6+0.1)";
//Outlet
Field[7] = MathEval;
Field[7].F = "2.0*(0.8*(x-7.4)^1.5+0.1)";
Field[8] = Min;
Field[8].FieldsList = {3,4,5,6,7};

//Sanity
//Field[7] = MathEval;
//Field[7].F = "0.08";
//Field[8] = Max;
//Field[8].FieldsList = {6,7};
//Field[1].F = "Cos(4*3.14*x) * Sin(4*3.14*y) / 10 + 0.101";
Background Field = 8;

Physical Line(Inlet) = {3};
Physical Line(Noslip) = {4};
Physical Line(Outlet) = {5};
Physical Line(Symm) = {6, 7};
Physical Line(Design) = {1, 2};
Physical Surface(0) = {9};
