#Routines for transporting information between different meshes

def MakeSurfBoundaryParts(mesh, gamma, boundary_parts):
    MaxDim = mesh.topology().dim()
    mapa = gamma.entity_map(MaxDim-1)
    SurfBoundaryParts=MeshFunction('size_t', gamma, 1)
    for ver in entities(gamma, MaxDim-1):
        i = mapa[ver.index()]
        SurfBoundaryParts[ver.index()] = boundary_parts[i]
    return SurfBoundaryParts

def VertexOnBoundary(mesh, boundary_parts, markers, InteriorOnly = False, Symmetry=[]):
    OnSubsurface = numpy.zeros(mesh.size_global(0), dtype=numpy.int)
    if InteriorOnly == False:
        for f in facets(mesh):
            if f.exterior() and not f.is_ghost():
                MyMarker = boundary_parts[f.index()]
                if MyMarker in markers:
                    for v in vertices(f):
                        OnSubsurface[v.global_index()] = 1
        SyncSum(OnSubsurface)
        OnSubsurface[numpy.nonzero(OnSubsurface)] = 1
    else:
        for v in vertices(mesh):
            OneWrong = False
            OneIncluded = False
            for f in facets(v):
                if f.exterior() and not f.is_ghost():
                    MyMarker = boundary_parts.array()[f.index()]
                    if MyMarker in markers:
                        OneIncluded = True
                    if MyMarker not in markers and MyMarker not in Symmetry:
                        OneWrong = True
            if OneIncluded == True and OneWrong == False:
                OnSubsurface[v.global_index()] = 1
            else:
                OnSubsurface[v.global_index()] = -1
        AllInfo = SyncAllGather(OnSubsurface)
        for i in range(len(OnSubsurface)):
            MyMin = numpy.min(AllInfo[i, :])
            MyMax = numpy.max(AllInfo[i, :])
            if MyMin < 0:
                OnSubsurface[i] = 0
            elif MyMax > 0:
                OnSubsurface[i] = 1
            else:
                OnSubsurface[i] = 0
    return OnSubsurface

#Take a 3D vector CG1-Function V and set it to zero everywhere execpt on faces of marker "marker"
def InjectOnSubSurface(V, mesh, boundary_parts, marker):
    MaxDim = mesh.topology().dim()
    gamma = BoundaryMesh(mesh, "exterior")
    SpaceS = FunctionSpace(gamma, "CG", 1)
    SpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
    V2 = Function(SpaceV)
        
    dof_min, dof_max = V2.vector().local_range()
    dof_min = int(dof_min/MaxDim)
    dof_max = int(dof_max/MaxDim)
    ll = dof_max - dof_min
    LocValues = numpy.zeros(MaxDim*ll)
        
    MyCorrectBoundary = VertexOnBoundary(mesh, boundary_parts, marker)
        
    mapa = gamma.entity_map(0)
    DofToVertex = dof_to_vertex_map(SpaceS) #maps to local/processor dofs
    for i in range(dof_min, dof_max):
        VertexID = int(DofToVertex[i-dof_min])
        #Vert = MeshEntity(gamma, 0, VertexID)
        #GlobalID = Vert.global_index()
        GlobalID = MeshEntity(mesh, 0, mapa[VertexID]).global_index()
        if MyCorrectBoundary[GlobalID] > 0:
            for j in range(0, MaxDim):
                value = V.vector()[MaxDim*i+j]
                LocValues[MaxDim*(i-dof_min)+j] = V.vector()[MaxDim*i+j]
    V2.vector().set_local(LocValues)
    V2.vector().apply('')
    return V2

def ReduceMeshFunctionToSurface(mesh, boundary_parts):
    mesh.init()
    MaxDim = mesh.topology().dim()
    VariableSurface = BoundaryMesh(mesh, "exterior")
    mapa = VariableSurface.entity_map(MaxDim-1)
    VariableBoundary_parts = MeshFunction('size_t', VariableSurface, MaxDim-1)
    #VariableBoundary_parts = CellFunction('size_t', VariableSurface)
    for ver in entities(VariableSurface, MaxDim-1):
        i = mapa[ver.index()]
        VariableBoundary_parts[ver.index()] = boundary_parts[i]
    return VariableBoundary_parts

def ReduceToSurface(VolumeGradient, SpaceType = "CG", SpaceOrder = 1):
    MaxDim = 1#VolumeGradient.function_space().dofmap().global_dimension()
    VolSpace = VolumeGradient.function_space()
    mesh = VolSpace.mesh()
    mesh.init()
    mesh.order()
    gamma = BoundaryMesh(mesh, "exterior")
    gamma.init()
    gamma.order()
    SurfSpace = FunctionSpace(gamma, SpaceType, SpaceOrder)
    SurfGrad = Function(SurfSpace)
    (sdmin, sdmax) = SurfGrad.vector().local_range()
    sdmin = int(sdmin/MaxDim)
    sdmax = int(sdmax/MaxDim)
    LocValues = numpy.zeros(MaxDim*(sdmax-sdmin))
    if SpaceType == "CG":
        VGlobal = numpy.zeros(len(VolumeGradient.vector()))
        (vdmin, vdmax) = VolumeGradient.vector().local_range()
        DofToVert = dof_to_vertex_map(VolSpace)
        for i in range(vdmax-vdmin):
            Vert = MeshEntity(VolumeGradient.function_space().mesh(), 0, DofToVert[i])
            GlobalIndex = Vert.global_index()
            value = VolumeGradient.vector()[i+vdmin]
            VGlobal[GlobalIndex] = value
        VGlobal = SyncSum(VGlobal)
        mapa = gamma.entity_map(0)
        DofToVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
        for i in range(sdmax-sdmin):
            VolVert = MeshEntity(mesh, 0, mapa[int(DofToVert[i])])
            GlobalIndex = VolVert.global_index()
            for j in range(MaxDim):
                value = VGlobal[MaxDim*GlobalIndex+j]
                LocValues[MaxDim*i+j] = value
    elif SpaceType == "DG":
        map = gamma.entity_map(2)
        if SpaceOrder == 0:
            for f in entities(gamma, 2):
                f_vol = MeshEntity(mesh, 2, map[f.index()])
                #Because we are looping physical boundary facets, there can be only one
                #volume element attached to this boundary facet:
                e_vol = entities(f_vol, 3).next()
                CellDofsSurf = SurfSpace.dofmap().cell_dofs(f.index())
                CellDofsVol = VolSpace.dofmap().cell_dofs(e_vol.index())
                LocValues[CellDofsSurf[0]] = VolumeGradient.vector()[CellDofsVol[0]]
        elif SpaceOrder == 1:
            TestSurf = MyProject(TestExpression, SurfSpace, "lu")
            TestVol = MyProject(TestExpression, VolSpace, "lu")
            for f in entities(gamma, 2):
                f_vol = MeshEntity(mesh, 2, map[f.index()])
                #Because we are looping physical boundary facets, there can be only one
                #volume element attached to this boundary facet:
                MyNumCells = 0
                ParCells = cells(f_vol)
                for e_vol in ParCells:
                    MyNumCells = MyNumCells + 1
                    CellDofsSurf = SurfSpace.dofmap().cell_dofs(f.index())
                    CellDofsVol = VolSpace.dofmap().cell_dofs(e_vol.index())
                    for i in CellDofsSurf:
                        TValueS = TestSurf.vector()[i]
                        Found = False
                        for j in CellDofsVol:
                            TValueVol = TestVol.vector()[j]
                            offset = abs(TValueVol - TValueS)
                            if offset < TestTol:
                                Found = True
                                LocValues[i] = VolumeGradient.vector()[j]
                                break
                        if Found == False:
                            print "WARNING: ReduceToSurfaceDG: DOF NOT FOUND"
                            print "i:",i,"TValueS =", TValueS
                            for j in CellDofsVol:
                                TValueVol = TestVol.vector()[j]
                                print "j:", j, "TValueVol", TValueVol, "diff:", abs(TValueVol - TValueS)
                                for v1 in vertices(f):
                                    print "v1.x(0)", v1.x(0), "v1.x(1)", v1.x(1), "v1.x(2)", v1.x(2), "value", abs(v1.x(0)) + abs(v1.x(1)) + abs(v1.x(2))
                                for v2 in vertices(e_vol):
                                    print "v2.x(0)", v2.x(0), "v2.x(1)", v2.x(1), "v2.x(2)", v2.x(2), "value", abs(v2.x(0)) + abs(v2.x(1)) + abs(v2.x(2))
                            exit(1)
                if MyNumCells != 1:
                    print "ERROR: Number of Parent Cells not as expected:", MyNumCells
                    exit(1)
        elif SpaceOrder > 1:
            print "ERROR: ReduceToSurfaceDG: WORKS ONLY FOR SpaceOrder <= 1"
            exit()
    SurfGrad.vector().set_local(LocValues)
    SurfGrad.vector().apply('')
    SurfGrad.rename("Surface", "dummy")
    return SurfGrad

def ReduceVectorToSurface(VolumeGradient, SpaceType="CG", SpaceOrder=1):
    mesh = VolumeGradient.function_space().mesh()
    MaxDim = mesh.topology().dim()
    gamma = BoundaryMesh(mesh, "exterior")
    SGradSpace = VectorFunctionSpace(gamma, SpaceType, SpaceOrder, MaxDim)
    SGrad = Function(SGradSpace)
    (sdmin, sdmax) = SGrad.vector().local_range()
    sdmin = int(sdmin/MaxDim)
    sdmax = int(sdmax/MaxDim)
    LocValues = numpy.zeros(MaxDim*(sdmax-sdmin))
    if SpaceType == "CG":
        VGlobal = numpy.zeros(len(VolumeGradient.vector()))
        (vdmin, vdmax) = VolumeGradient.vector().local_range()
        vdmin = int(vdmin/MaxDim)
        vdmax = int(vdmax/MaxDim)
        DofToVert = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
        for i in range(vdmax-vdmin):
            Vert = MeshEntity(mesh, 0, DofToVert[i])
            GlobalIndex = Vert.global_index()
            for j in range(MaxDim):
                value = VolumeGradient.vector()[MaxDim*(i+vdmin)+j]
                VGlobal[MaxDim*GlobalIndex+j] = value
        VGlobal = SyncSum(VGlobal)
        mapa = gamma.entity_map(0)
        DofToVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
        for i in range(sdmax-sdmin):
            VolVert = MeshEntity(mesh, 0, mapa[int(DofToVert[i])])
            GlobalIndex = VolVert.global_index()
            for j in range(MaxDim):
                value = VGlobal[MaxDim*GlobalIndex+j]
                LocValues[MaxDim*i+j] = value
    else:
        print "ReduceVectorToSurface: ONLY CG 1 SUPPORTED"
        exit(1)
    SGrad.vector().set_local(LocValues)
    SGrad.vector().apply('')
    return SGrad

#take a CG1 vector f defined on a surface mesh and return a volume vector with same values on boundary but zero in volume
def InjectVectorFromSurface(f, mesh):
    SpaceOrder = 1
    SpaceType = "CG"
    MaxDim = 3
    gamma = BoundaryMesh(mesh, "exterior")
    SpaceS = FunctionSpace(mesh, SpaceType, SpaceOrder)
    SpaceV = VectorFunctionSpace(mesh, SpaceType, SpaceOrder, MaxDim)
    F = Function(SpaceV)
    LocValues = numpy.zeros(F.vector().local_size())
    map = gamma.entity_map(0)
    d2v = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
    v2d = vertex_to_dof_map(SpaceS)
    for i in range(f.vector().local_size()/MaxDim):
        GVertID = Vertex(gamma, d2v[i]).index()
        PVertID = map[GVertID]
        PDof = v2d[PVertID]
        for j in range(MaxDim):
            value = f.vector()[MaxDim*i+j]
            LocValues[PDof*MaxDim+j] = value
    F.vector().set_local(LocValues)
    F.vector().apply("")
    return F

def ExtractSubsurface_old(marker, mesh, boundary_parts):
    gamma = BoundaryMesh(mesh, "exterior")
    mapa = gamma.entity_map(2)
    boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    for ver in entities(gamma, 2):
        i = mapa[ver.index()]
        if boundary_parts[i] in marker:
            boundaryboundary_parts[ver.index()] = 1
        else:
            boundaryboundary_parts[ver.index()] = 0
    gammagamma = SubMesh(gamma, boundaryboundary_parts, 1)
    return (gammagamma, boundaryboundary_parts)

#Extract Subsurface with ID marker and return global Surface Cell (= facet) to source global volume cell (= element)
def ExtractSubsurface(marker, mesh, boundary_parts):
    mesh.init()
    mesh.order()
    code="""
        #include <dolfin/mesh/MeshPartitioning.h>
        namespace dolfin {
        void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
        {
        MeshPartitioning::build_distributed_mesh(*mesh);
        }
        };
        """
    build_distributed_mesh = compile_extension_module(code).build_distributed_mesh
        
    MaxDim = mesh.topology().dim()
    GBP = GlobalizeMeshFunction(boundary_parts)
    NumVertexGlobal = mesh.size_global(0)
    #NumCellsGlobal = mesh.size_global(MaxDim)
    coords = mesh.coordinates()
    CoordsGlobal = numpy.zeros([NumVertexGlobal, MaxDim])
    NumProcGlobal = numpy.zeros(NumVertexGlobal)
    for v in vertices(mesh):
        x = numpy.array(coords[v.index()])
        CoordsGlobal[v.global_index(),:] = x
        NumProcGlobal[v.global_index()] = 1.0
    SyncSum(CoordsGlobal)
    SyncSum(NumProcGlobal)
    FacetsOnBoundary = numpy.zeros(1, dtype=numpy.uintp)
    for f in facets(mesh):
        if f.is_ghost() == False:
            if MPI.size(mpi_comm_world()) == 1:
                ID = f.index()
            else:
                ID = f.global_index()
            if GBP[ID] in marker:
                FacetsOnBoundary[0] += 1
    FacetsOnBoundary = SyncAllGather(FacetsOnBoundary)
    NumFacets = numpy.sum(numpy.sum(FacetsOnBoundary))
    MyStartIndex = 0
    for i in range(MPI.rank(mpi_comm_world())):
        MyStartIndex += FacetsOnBoundary[0,i]
    #Array [Global Facet ID, global Vertices full]
    BVertexID = numpy.zeros([NumFacets, MaxDim], dtype=numpy.uintp)
    #The return map global Surface Facet to global Volume Cell
    SFacet2VCell = numpy.zeros(NumFacets, dtype=numpy.uintp)
    SFacetNormal = numpy.zeros([NumFacets, MaxDim])
    #For new boundary_markers
    SFacetBP = numpy.zeros(NumFacets, numpy.uintp)
    MyAdded = 0
    MyStartIndex = int(MyStartIndex)
    MyAdded = int(MyAdded)
    for f in facets(mesh):
        if MPI.size(mpi_comm_world()) == 1:
            ID = f.index()
        else:
            ID = f.global_index()
        if GBP[ID] in marker and f.is_ghost() == False:
            #For new boundary_markers
            SFacetBP[MyStartIndex + MyAdded] = GBP[ID]
            VID = 0
            for v in vertices(f):
                BVertexID[MyStartIndex + MyAdded, VID] = v.global_index()
                VID += 1
            #Only for the SFacet2VCell Map:
            for j in range(MaxDim):
                SFacetNormal[MyStartIndex + MyAdded, j] = f.normal(j)
            for c in cells(f):
                SFacet2VCell[MyStartIndex + MyAdded] = c.global_index()
                break
            MyAdded += 1
    SyncSum(BVertexID)
    SyncSum(SFacet2VCell)
    SyncSum(SFacetBP)
    #renumber boundary vertices consistently
    TMP = numpy.zeros(mesh.size_global(0), dtype=numpy.uintp)
    for i in range(NumFacets):
        for j in range(MaxDim):
            GID = BVertexID[i,j]
            TMP[GID] = 1
    BVertex2VVertex = numpy.zeros(numpy.sum(TMP), dtype=numpy.uintp)
    VVertex2BVertex = numpy.zeros(mesh.size_global(0), dtype=numpy.uintp)
    NumBVertices = 0
    for i in range(len(TMP)):
        if TMP[i] > 0:
            BVertex2VVertex[NumBVertices] = i
            VVertex2BVertex[i] = NumBVertices
            NumBVertices += 1
    MyBoundaryMesh = Mesh()
    editor = MeshEditor()
    editor.open(MyBoundaryMesh, MaxDim-1, MaxDim)
    editor.init_vertices_global(NumBVertices, NumBVertices)
    editor.init_cells_global(NumFacets, NumFacets)
    #Parallel bug in FEniCS 1.7dev:
    #BEGIN SERIAL PART: Build mesh on node 0 only!
    if MPI.rank(mpi_comm_world()) == 0:
        for i in range(len(BVertex2VVertex)):
            j = BVertex2VVertex[i]
            x = CoordsGlobal[j,:]/NumProcGlobal[j]
            editor.add_vertex_global(i, i, x)
        for i in range(len(BVertexID)):
            VertIndex = VVertex2BVertex[BVertexID[i, :]]
            editor.add_cell(i, VertIndex)
    editor.close()
    build_distributed_mesh(MyBoundaryMesh)
    MyBoundaryMesh.init()
    MyBoundaryMesh.order()
        
    #Constract Facet Normal
    SyncSum(SFacetNormal)
    NormalSpace = VectorFunctionSpace(MyBoundaryMesh, "DG", 0, MaxDim)
    FacetNormal = Function(NormalSpace)
    LocValues = numpy.zeros(FacetNormal.vector().local_size())
    for c in cells(MyBoundaryMesh):
        GID = c.global_index()
        i = 0
        for dof in NormalSpace.dofmap().cell_dofs(c.index()):
            value = SFacetNormal[GID][i]
            LocValues[dof] = value
            i += 1
    FacetNormal.vector().set_local(LocValues)
    FacetNormal.vector().apply("")
    FacetNormal.rename("Normal", "dummy")
        
    BBP = MeshFunction("size_t", MyBoundaryMesh, MaxDim-1)
    for f in cells(MyBoundaryMesh):
        BBP.array()[f.index()] = SFacetBP[f.global_index()]
    return MyBoundaryMesh, BBP, FacetNormal, SFacet2VCell

def ReduceFunctionToSubsurface(h, gammagamma, type, degree):
    vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
    SubSpace = FunctionSpace(gammagamma, type, degree)
    ParSpace = h.function_space()
    hb = Function(SubSpace)
    LocValues = numpy.zeros(hb.vector().local_size())
    if type == "CG":
        d2v = dof_to_vertex_map(SubSpace)
        v2d = vertex_to_dof_map(ParSpace)
        for i in range(hb.vector().local_size()):
            ver = d2v[i]
            SubIndex = Vertex(gammagamma, ver).index()
            ParIndex = vertex_indices[SubIndex]
            ParDof = v2d[ParIndex]
            value = h.vector()[ParDof]
            LocValues[i] = value
    elif type == "DG":
        cell_indices = gammagamma.data().array("parent_cell_indices", 2)
        if degree == 0:
            for f in entities(gammagamma, 2):
                CIndex = f.index()
                PIndex = cell_indices[CIndex]
                CCellDofs = SubSpace.dofmap().cell_dofs(CIndex)
                PCellDofs = ParSpace.dofmap().cell_dofs(PIndex)
                LocValues[CCellDofs[0]] = h.vector()[PCellDofs[0]]
        elif degree == 1:
            TestC = interpolate(TestExpression, SubSpace)
            TestP = interpolate(TestExpression, ParSpace)
            for f in entities(gammagamma, 2):
                CIndex = f.index()
                PIndex = cell_indices[CIndex]
                CCellDofs = SubSpace.dofmap().cell_dofs(CIndex)
                PCellDofs = ParSpace.dofmap().cell_dofs(PIndex)
                for i in CCellDofs:
                    CValue = TestC.vector()[i]
                    Found = False
                    for j in PCellDofs:
                        PValue = TestP.vector()[j]
                        if abs(CValue - PValue) < TestTol:
                            Found = True
                            LocValues[i] = h.vector()[j]
                            break
                    if Found == False:
                        print "ReduceFunctionToSubsurface DG 1: ERROR: DOF NOT FOUND"
                        exit(1)
        else:
            print "ReduceFunctionToSubsurface: DG DEGREE TOO HIGH!"
            exit(1)
    else:
        print "ReduceFunctionToSubsurface: Unrecognized Type!"
        exit()
    hb.vector().set_local(LocValues)
    hb.vector().apply("")
    #HB = FunctionSpace(gammagamma, type, degree, 3)
    #hb = interpolate(h, HB)
    #hb.name = h.name
    return hb

def ReduceVectorToSubsurface(h, gammagamma, type, degree, dim):
    vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
    SubSpace = VectorFunctionSpace(gammagamma, type, degree, dim)
    ParSpace = h.function_space()
    mesh = ParSpace.mesh()
    hb = Function(SubSpace)
    LocValues = numpy.zeros(hb.vector().local_size())
    if type == "CG":
        d2v = dof_to_vertex_map(FunctionSpace(gammagamma, type, degree))
        v2d = vertex_to_dof_map(FunctionSpace(mesh, type, degree))
        for i in range(hb.vector().local_size()/dim):
            ver = d2v[i]
            SubIndex = Vertex(gammagamma, ver).index()
            ParIndex = vertex_indices[SubIndex]
            ParDof = v2d[ParIndex]
            for j in range(dim):
                value = h.vector()[ParDof*dim+j]
                LocValues[i*dim+j] = value
    hb.vector().set_local(LocValues)
    hb.vector().apply("")
    #HB = FunctionSpace(gammagamma, type, degree, 3)
    #hb = interpolate(h, HB)
    #hb.name = h.name
    return hb

def ExpandFunctionFromSubsurface(hb, omega, gammagamma, type, degree):
    vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
    ParSpace = FunctionSpace(omega, type, degree)
    SubSpace = hb.function_space()
    d2v = dof_to_vertex_map(SubSpace)
    v2d = vertex_to_dof_map(ParSpace)
    h = Function(ParSpace)
    LocValues = numpy.zeros(h.vector().local_size())
    for i in range(hb.vector().local_size()):
        value = hb.vector()[i]
        ver = d2v[i]
        SubIndex = Vertex(gammagamma, ver).index()
        ParIndex = vertex_indices[SubIndex]
        ParDof = v2d[ParIndex]
        LocValues[ParDof] = value
    h.vector().set_local(LocValues)
    h.vector().apply("")
    #HB = FunctionSpace(gammagamma, type, degree, 3)
    #hb = interpolate(h, HB)
    #hb.name = h.name
    return h

def ExpandVectorFromSubsurface(hb, omega, gammagamma, type, degree, dim):
    vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
    ParSpace = VectorFunctionSpace(omega, type, degree, dim)
    SubSpace = VectorFunctionSpace(gammagamma, type, degree, dim)
    d2v = dof_to_vertex_map(FunctionSpace(gammagamma, type, degree))
    v2d = vertex_to_dof_map(FunctionSpace(omega, type, degree))
    h = Function(ParSpace)
    LocValues = numpy.zeros(h.vector().local_size())
    for i in range((hb.vector().local_size())/dim):
        ver = d2v[i]
        SubIndex = Vertex(gammagamma, ver).index()
        ParIndex = vertex_indices[SubIndex]
        ParDof = v2d[ParIndex]
        for j in range(dim):
            value = hb.vector()[dim*i+j]
            LocValues[dim*ParDof+j] = value
    h.vector().set_local(LocValues)
    h.vector().apply("")
    #HB = FunctionSpace(gammagamma, type, degree, 3)
    #hb = interpolate(h, HB)
    #hb.name = h.name
    return h

def ReduceVectorFunctionToSubsurface(h, omega, gammagamma, type, degree):
    #Extract Subsurface with ID marker
    #gamma = BoundaryMesh(omega, "exterior")
    #mapa = gamma.entity_map(2)
    #boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    #for ver in entities(gamma, 2):
    #   i = mapa[ver.index()]
    #   boundaryboundary_parts[ver.index()] = boundary_parts[i]
    #gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)
    HB = VectorFunctionSpace(gammagamma, type, degree, 3)
    hb = interpolate(h, HB)
    hb.name = h.name
    #H=VectorFunctionSpace(omega, 'DG', degree,3)
    #psi=TestFunction(HB)
    #solve(inner(h-hb,psi)*dss(4) == 0, hb)
    return hb

def ReduceTensorFunctionToSubsurface(h, omega, boundary_parts, marker, degree):
    #Extract Subsurface with ID marker
    gamma = BoundaryMesh(omega, "exterior")
    mapa = gamma.entity_map(2)
    boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    for ver in entities(gamma, 2):
        i = mapa[ver.index()]
        boundaryboundary_parts[ver.index()] = boundary_parts[i]
    gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)
        
    HB = TensorFunctionSpace(gammagamma, 'DG', degree, ((3,3)))
    #hb = Function(HB)
    hb = interpolate(h, HB)
    return hb

def InterpolateExpressionOnSubsurface(expression, omega, boundary_parts, marker, type, degree):
    #Extract Subsurface with ID marker
    gamma = BoundaryMesh(omega, "exterior")
    mapa = gamma.entity_map(2)
    boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    for ver in entities(gamma, 2):
        i = mapa[ver.index()]
        boundaryboundary_parts[ver.index()] = boundary_parts[i]
    gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)
        
    N=FunctionSpace(gammagamma, type, degree)
    F=Function(N)
    T=TestFunction(N)
    solve(T*expression*dx - T*F*dx == 0, F)
    return F
    
def InterpolateVectorExpressionOnSubsurface(expression, omega, boundary_parts, marker, type, degree):
    #Extract Subsurface with ID marker
    gamma = BoundaryMesh(omega, "exterior")
    mapa = gamma.entity_map(2)
    boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    for ver in entities(gamma, 2):
        i = mapa[ver.index()]
        boundaryboundary_parts[ver.index()] = boundary_parts[i]
    gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)
        
    N=VectorFunctionSpace(gammagamma, type, degree, 3)
    F=Function(N)
    T=TestFunctions(N)
    solve(inner(T, expression)*dx - inner(T,F)*dx == 0, F)
    return F

def GlobalizeMesh(mesh):
    TopDim = mesh.topology().dim()
    GeoDim = mesh.geometry().dim()
    NumVertexGlobal = mesh.size_global(0)
    NumCellsGlobal = mesh.size_global(TopDim)
    coords = mesh.coordinates()
    CoordsGlobal = numpy.zeros([NumVertexGlobal, GeoDim])
    NumProcGlobal = numpy.zeros(NumVertexGlobal)
    for v in vertices(mesh):
        x = numpy.array(coords[v.index()])
        CoordsGlobal[v.global_index(),:] = x
        NumProcGlobal[v.global_index()] = 1.0
    SyncSum(CoordsGlobal)
    SyncSum(NumProcGlobal)
    if min(NumProcGlobal==0):
        dprint("ERROR: THERE IS A VERTEX NOT BELONGING TO ANY PROCESSOR")
        exit(1)
    for IndexGlobal in range(NumVertexGlobal):
        CoordsGlobal[IndexGlobal,:] = CoordsGlobal[IndexGlobal,:]/NumProcGlobal[IndexGlobal]
    CellsGlobal = numpy.zeros([NumCellsGlobal, TopDim+1], dtype=numpy.uintp)
    for c in cells(mesh):
        LocVertexID = 0
        for v in vertices(c):
            CellsGlobal[c.global_index(), LocVertexID] = v.global_index()
            LocVertexID += 1
    SyncSum(CellsGlobal)
    #New: Also globalize Facets:
    NumFacetsGlobal = mesh.size_global(TopDim-1)
    NumProcFacets = numpy.zeros(NumFacetsGlobal)
    FacetsGlobal = numpy.zeros([NumFacetsGlobal, TopDim], dtype=numpy.uintp)
    for f in facets(mesh):
        LocVertexID = 0
        #For some VERY strange reason, faces in SymmetricMesh segfault upon global_index() call
        #And ONLY on 1 processor...
        if MPI.size(mpi_comm_world()) == 1:
            f_index = f.index()
        else:
            f_index = f.global_index()
        for v in vertices(f):
            FacetsGlobal[f_index, LocVertexID] = v.global_index()
            LocVertexID += 1
        NumProcFacets[f_index] = 1.0
    SyncSum(FacetsGlobal)
    SyncSum(NumProcFacets)
    for IndexGlobal in range(NumFacetsGlobal):
        FacetsGlobal[IndexGlobal,:] = FacetsGlobal[IndexGlobal,:]/NumProcFacets[IndexGlobal]
    return (CoordsGlobal, CellsGlobal, FacetsGlobal)

def Mirror(mesh, plane, Offset = 0.0):
    code="""
        #include <dolfin/mesh/MeshPartitioning.h>
            
        namespace dolfin {
        void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
        {
        MeshPartitioning::build_distributed_mesh(*mesh);
        }
        };
        """
    build_distributed_mesh = compile_extension_module(code).build_distributed_mesh

    MaxDim = mesh.topology().dim()
    mesh.init()
    mesh.order()
    (CoordsGlobal, CellsGlobal) = GlobalizeMesh(mesh)
    OnSymmetryGlobal = 0
    #Determine vertices on mirror plane (global)
    for i in range(mesh.size_global(0)):
        #if OnInterfaceGlobal[i] > 0.0:
        if(bool(near(CoordsGlobal[i, plane], Offset, TestTol))):
            OnSymmetryGlobal += 1
    #Global Number of Vertices on Symmetry
    NumVertexGlobal = mesh.size_global(0)
    NumSVertexGlobal = 2*NumVertexGlobal - OnSymmetryGlobal
    NumCellsGlobal = mesh.size_global(MaxDim)
        
    SymmetricMesh = Mesh()
    editor = MeshEditor()
    editor.open(SymmetricMesh, MaxDim, MaxDim)
    editor.init_vertices_global(NumSVertexGlobal, NumSVertexGlobal)
    editor.init_cells_global(2*NumCellsGlobal, 2*NumCellsGlobal)
    #MirrorVertexMap: points from global id of original vertex to global id of mirrored vertex
    #identity if point on mirror plane
    MirrorVertexMap = numpy.zeros(NumVertexGlobal, dtype=numpy.uintp)
    MirrorVertexMapInv = numpy.zeros(NumSVertexGlobal, dtype=numpy.uintp)
    #Parallel bug in FEniCS 1.7dev:
    #BEGIN SERIAL PART: Build mesh on node 0 only!
    if MPI.rank(mpi_comm_world()) == 0:
        #compute new global indices for the new points on the mirrored half:
        SIndexGlobal = numpy.zeros(NumVertexGlobal, dtype="P")
        OffsetIndex = 0
        for i in range(mesh.size_global(0)):
            if(bool(near(CoordsGlobal[i, plane], Offset, TestTol))):
                OffsetIndex = OffsetIndex + 1
                SIndexGlobal[i] = OffsetIndex
        NumAdded = 0
        for IndexGlobal in range(NumVertexGlobal):
            x = CoordsGlobal[IndexGlobal,:]
            editor.add_vertex_global(IndexGlobal, IndexGlobal, x)
            MirrorVertexMap[IndexGlobal] = IndexGlobal
            MirrorVertexMapInv[IndexGlobal] = IndexGlobal
            if not bool(near(x[plane], Offset, TestTol)):
                #Compute Mirror coordinates:
                x_new = numpy.empty(MaxDim)
                for i in range(MaxDim):
                    x_new[i] = x[i]
                x_new[plane] = -(x_new[plane]-Offset)+Offset
                editor.add_vertex_global(NumVertexGlobal + NumAdded, NumVertexGlobal + NumAdded, x_new)
                MirrorVertexMap[IndexGlobal] = NumVertexGlobal + NumAdded
                MirrorVertexMapInv[NumVertexGlobal + NumAdded] = IndexGlobal
                NumAdded = NumAdded + 1
        for Index in range(NumCellsGlobal):
            VertIndex = CellsGlobal[Index, :]
            editor.add_cell(Index, VertIndex)
            editor.add_cell(Index+NumCellsGlobal, MirrorVertexMap[VertIndex])
    #END SERIAL PART
    editor.close()
    SyncSum(MirrorVertexMap)
    SyncSum(MirrorVertexMapInv)
    build_distributed_mesh(SymmetricMesh)
    SymmetricMesh.init()
    SymmetricMesh.order()
    return (SymmetricMesh, MirrorVertexMapInv)

#Return map "local facet ID Symmetric -> global facet ID original"
def MatchMirrorFaces(mesh, SymmetricMesh, MirrorVertexMap):
    MaxDim = mesh.topology().dim()
    #Make an array: [Global Cell, Global Facets, Global Vertices]
    NumCellsGlobal = mesh.size_global(MaxDim)
    FacetsGlobal = numpy.zeros([NumCellsGlobal, MaxDim+1, MaxDim], dtype=numpy.uintp)
    FacetsGlobalIDs = numpy.zeros([NumCellsGlobal, MaxDim+1], dtype=numpy.uintp)
    for c in cells(mesh):
        LocFacetID = 0
        for f in facets(c):
            if f.is_ghost() == False:
                LocVertexID = 0
                MyVertex = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
                for v in vertices(f):
                    MyVertex[LocVertexID] = v.global_index()
                    LocVertexID = LocVertexID+1
                FacetsGlobal[c.global_index(), LocFacetID,:] = MyVertex
                #For some VERY strange reason, faces in SymmetricMesh segfault upon global_index() call
                #And ONLY on 1 processor...
                if MPI.size(mpi_comm_world()) == 1:
                    FacetsGlobalIDs[c.global_index(), LocFacetID] = f.index()
                else:
                    FacetsGlobalIDs[c.global_index(), LocFacetID] = f.global_index()
                LocFacetID += 1
    #no division needed, each cell is only part of one processor!
    SyncSum(FacetsGlobal)
    SyncSum(FacetsGlobalIDs)
    #CopyFacetMap = MeshFunction("size_t", mesh, MaxDim-1)
    MirrorFacetMap = MeshFunction("size_t", SymmetricMesh, MaxDim-1)
    for i in range(len(MirrorFacetMap.array())):
        MirrorFacetMap.array()[i] = 0
    for f in facets(SymmetricMesh):
        if f.is_ghost() == True:
            continue
        #CFound = False
        MFound = False
        #MyVertexC = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
        MyVertexM = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
        i = 0
        for v in vertices(f):
            #MyVertexC[i] = v.global_index()
            MyVertexM[i] = MirrorVertexMap[v.global_index()]
            i = i+1
        #MyVertexC = sorted(MyVertexC)
        MyVertexM = sorted(MyVertexM)
        for e in cells(f):
            GID_C = e.global_index()
            if GID_C < mesh.size_global(MaxDim):
                GID_M = GID_C
            else:
                GID_M = GID_C - mesh.size_global(MaxDim)
            for j in range(e.num_entities(MaxDim-1)):
                MM = sorted(FacetsGlobal[GID_M, j, :])
                if MyVertexM == MM:
                    MirrorFacetMap.array()[f.index()] = FacetsGlobalIDs[GID_M, j]
                    MFound = True
                    break
            break
        if MFound == False:
            dprint("MatchMirrorFaces: Mirror Facet not found!")
            for e in cells(f):
                GID_C = e.global_index()
                if GID_C < mesh.size_global(MaxDim):
                    GID_M = GID_C
                else:
                    GID_M = GID_C - mesh.size_global(MaxDim)
                for j in range(e.num_entities(MaxDim-1)):
                    MM = sorted(FacetsGlobal[GID_M, j, :])
                    print "Matching", MyVertexM, MM
        exit(1)
    return MirrorFacetMap

def StoreMirrorMeshHDD(FileName, Mesh, boundary_parts, SymmetricMesh, SymmetricBoundaryParts, CFM, MFM):
    File = HDF5File(mpi_comm_world(), FileName, "w")
    File.write(Mesh, "Mesh")
    File.write(boundary_parts, "boundary_parts")
    File.write(SymmetricMesh, "SymmetricMesh")
    File.write(SymmetricBoundaryParts, "SymmetricBoundaryParts")
    File.write(CFM, "CopyFacetMap")
    File.write(MFM, "MirrorFacetMap")

def LoadMirrorMeshHDD(FileName):
    File = HDF5File(mpi_comm_world(), FileName, "r")
    mesh = Mesh()
    File.read(mesh, "Mesh", False)
    MaxDim = mesh.topology().dim()
    pprint("MaxDim %d"%MaxDim)
    boundary_parts = MeshFunction('size_t', mesh, MaxDim-1)
    File.read(boundary_parts, "boundary_parts")
    SymmetricMesh = Mesh()
    File.read(SymmetricMesh, "SymmetricMesh", False)
    SymmetricBoundaryParts = MeshFunction('size_t', SymmetricMesh, MaxDim-1)
    File.read(SymmetricBoundaryParts, "SymmetricBoundaryParts")
    CFM = MeshFunction('size_t', mesh, MaxDim-1)
    File.read(CFM, "CopyFacetMap")
    MFM = MeshFunction('size_t', mesh, MaxDim-1)
    File.read(MFM, "MirrorFacetMap")
    return (mesh, boundary_parts, SymmetricMesh, SymmetricBoundaryParts, CFM, MFM)

def GlobalizeMeshFunction(mf):
    MaxDim = mf.dim()
    mesh = mf.mesh()
    GlobalValues = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
    GlobalNumProc = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
    for e in entities(mesh, MaxDim):
        IDL = e.index()
        #strange bug in mesh generator meshes...
        if MPI.size(mpi_comm_world()) == 1 and MaxDim == 2:
            IDG = IDL
        else:
            IDG = e.global_index()
        GlobalValues[IDG] = mf.array()[IDL]
        GlobalNumProc[IDG] += 1
    SyncSum(GlobalValues)
    SyncSum(GlobalNumProc)
    return GlobalValues/GlobalNumProc

def MirrorMeshFunction(SymmetricMesh, boundary_parts, MirrorFacetMap, MarkerToRemove = -1):
    MaxDim = boundary_parts.dim()
    BP_global = GlobalizeMeshFunction(boundary_parts)
    SymmetryBoundaryMap = MeshFunction('size_t', SymmetricMesh, MaxDim)
    for i in range(len(SymmetryBoundaryMap.array())):
        SymmetryBoundaryMap.array()[i] = 0
    for f in facets(SymmetricMesh):
        if f.is_ghost() == True:
            continue
        LID = f.index()
        value = BP_global[MirrorFacetMap.array()[LID]]
        if value in MarkerToRemove:
            value = 0
        SymmetryBoundaryMap.array()[LID] = value
    return SymmetryBoundaryMap

#return an array A[global_cell_id] = global_dofs
def GlobalizeDofMap(SymmetricMesh, SpaceType, SpaceOrder):
    MaxDim = SymmetricMesh.topology().dim()
    TestFunctionSpaceM = FunctionSpace(SymmetricMesh, SpaceType, SpaceOrder)
    MyDofMap = TestFunctionSpaceM.dofmap()
    DofsPerElement = MyDofMap.max_cell_dimension()
    GlobalDimension = MyDofMap.global_dimension()
    CellDofsGlobal = numpy.zeros([SymmetricMesh.size_global(MaxDim), DofsPerElement], dtype=numpy.uintp)
    for c in cells(SymmetricMesh):
        CellDofs = MyDofMap.cell_dofs(c.index())
        for j in range(DofsPerElement):
            CellDofsGlobal[c.global_index(), j] = MyDofMap.local_to_global_index(CellDofs[j])
    SyncSum(CellDofsGlobal)
    return CellDofsGlobal
    
def GlobalizeFunction(f):
    MyDofMap = f.function_space().dofmap()
    NumDofsGlobal = MyDofMap.global_dimension()
    GlobalValues = numpy.zeros(NumDofsGlobal)
    GlobalProcs = numpy.zeros(NumDofsGlobal)
    (sdmin, sdmax) = f.vector().local_range()
    for i in range(sdmax-sdmin):
        value = f.vector()[i]
        gindex = MyDofMap.local_to_global_index(i)
        GlobalValues[gindex] = value
        GlobalProcs[gindex] = 1.0
    SyncSum(GlobalValues)
    SyncSum(GlobalProcs)
    return GlobalValues/GlobalProcs

#Make DofMapping for copying from SourceMesh to DestinationMesh
#so mapping points from DestinationMesh to SourceMesh
def MakeInterMeshDofMap(SourceMesh, DestinationMesh, SpaceType, SpaceOrder, CellMap = None):
    #transmit symmetric mesh
    #main iterate over original mesh
    #but return mapping from DestinationMesh mesh (global) to SourceMesh mesh (global)...
    CellDofsGlobal = GlobalizeDofMap(SourceMesh, SpaceType, SpaceOrder)
    DimSource = SourceMesh.topology().dim()
    DimDesination = DestinationMesh.topology().dim()
    TestFunctionSpaceS = FunctionSpace(SourceMesh, SpaceType, SpaceOrder)
    TestFunctionSpaceD = FunctionSpace(DestinationMesh, SpaceType, SpaceOrder)
    TestXS = interpolate(TestExpression, TestFunctionSpaceS)
    TestXS = GlobalizeFunction(TestXS)
    TestXD = interpolate(TestExpression, TestFunctionSpaceD)
    OwnerRange = TestFunctionSpaceD.dofmap().ownership_range()
    InvMap = numpy.zeros([2,TestFunctionSpaceD.dofmap().local_dimension("owned")], dtype=numpy.uintp)
    for e in cells(DestinationMesh):
        LID_Destination = e.index()
        GID_Destination = e.global_index()
            
        IsMirrored = 0
        #no map, assume we want to fill the mirror mesh
        if CellMap == None:
            if GID_Destination() < mesh.size_global(MaxDim):
                GID_Source = GID_Destination
            else:
                GID_Source = GID_Destination - SourceMesh.size_global(DimSource)
                IsMirrored = 1
        else:
            GID_Source = CellMap[GID_Destination]
        
        SourceDofs = CellDofsGlobal[GID_Source,:]
        DestinationDofs = TestFunctionSpaceD.dofmap().cell_dofs(LID_Destination)
        for DDof in DestinationDofs:
            IsOwned = OwnerRange[0] <= TestFunctionSpaceD.dofmap().local_to_global_index(DDof) and TestFunctionSpaceD.dofmap().local_to_global_index(DDof) < OwnerRange[1]
            if IsOwned:
                if InvMap[0, DDof] != 0:
                    continue
                DValue = TestXD.vector()[DDof]
                DDofFound = False
                for SDof in SourceDofs:
                    SValue = TestXS[SDof]
                    if abs(DValue-SValue) < TestTol or SpaceOrder == 0:
                        InvMap[0][DDof] = SDof
                        InvMap[1][DDof] = IsMirrored
                        DDofFound = True
                        break
                if DDofFound == False:
                    dprint("ERROR: MakeInterMeshDofMap: Destination DOF not found!")
                    dprint("Processing: %d %d"%(LID_Destination, GID_Source))
                    dprint("Looking for valueP: %e"%DValue)
                    for SDof in SourceDofs:
                        SValue = TestXS[SDof]
                        dprint("Found valueC: %e"%SValue)
                    exit(1)
    return InvMap

#This is a bit complicated...
#Boundary Mesh Face -> Full Mesh Face -> Mirror Mesh Face -> Mirror Boundary Mesh Face
def MakeMirrorFaceDofMap(mesh, SymmetricMesh, CopyFaceMap, MirrorFaceMap, SpaceType, SpaceOrder, marker):
    gamma = BoundaryMesh(mesh, "exterior")
    gammaM = BoundaryMesh(SymmetricMesh, "exterior")
    MapaM = gammaM.entity_map(2)
    MapaMInv = MeshFunction('size_t', SymmetricMesh, 2)
    for i in range(len(MapaMInv.array())):
        MapaMInv[i] = 0
    for i in range(len(MapaM)):
        MapaMInv[MapaM[i]] = i
    TestFunctionSpace = FunctionSpace(gamma, SpaceType, SpaceOrder)
    TestFunctionSpaceM = FunctionSpace(gammaM, SpaceType, SpaceOrder)
    TestExpression = TestExpression
    TestX = interpolate(TestExpression, TestFunctionSpace)
    TestXM = interpolate(TestExpression, TestFunctionSpaceM)
    dm = TestFunctionSpace.dofmap()
    dmMirror = TestFunctionSpaceM.dofmap()
    mapa = gamma.entity_map(2)
    CopyFaceDofMap = range(len(TestX.vector()))
    MirrorFaceDofMap = range(len(TestX.vector()))
    for i in range(len(TestX.vector())):
        CopyFaceDofMap[i] = -1
        MirrorFaceDofMap[i] = -1
    for MyFace in entities(gamma, 2):
        FaceIndex = MyFace.index()
        i = mapa[FaceIndex]
        if boundary_parts[i] == marker:
            CopyID = MapaMInv[CopyFaceMap[i]]
            MirrorID = MapaMInv[MirrorFaceMap[i]]
            #check vertices
            CopyFace = Face(gammaM, CopyID)
            MyVertex = MyFace.entities(0)
            dofs = dm.cell_dofs(FaceIndex)
            CopyDofs = dmMirror.cell_dofs(CopyID)
            MirrorDofs = dmMirror.cell_dofs(MirrorID)
            for j in range(len(dofs)):
                value = TestX.vector()[dofs[j]]
                FoundCopy = False
                for k in range(len(dofs)):
                    ValueCopy = TestXM.vector()[CopyDofs[k]]
                    if value == ValueCopy:
                        CopyFaceDofMap[dofs[j]] = CopyDofs[k]
                        FoundCopy = True
                        break
                if FoundCopy == False:
                    print "COPY FACE DOF NOT FOUND!"
                    exit()
                FoundMirror = False
                for k in range(len(dofs)):
                    ValueMirror = TestXM.vector()[MirrorDofs[k]]
                    if value == ValueMirror:
                        MirrorFaceDofMap[dofs[j]] = MirrorDofs[k]
                        FoundMirror = True
                        break
                if FoundMirror == False:
                    print "MIRROR FACE DOF NOT FOUND!"
                    exit()
    return CopyFaceDofMap, MirrorFaceDofMap

#OrigDof is mapping from local destination dof to global source dof
#Push function from SourceMesh to DestinationMesh
def PushInterMesh(SourceMesh, DestinationMesh, OrigDof, fSource, Type, Deg, plane=-1):
    MyName = fSource.name()
    dim = fSource.function_space().element().value_dimension(0)
    fSource = GlobalizeFunction(fSource)
    if dim == 1:
        DestinationSpace = FunctionSpace(DestinationMesh, Type, Deg)
    else:
        DestinationSpace = VectorFunctionSpace(DestinationMesh, Type, Deg, dim)
    fDestination = Function(DestinationSpace)
    VecEnd = fDestination.vector().local_size()/dim
    if not numpy.shape(OrigDof)[1] == VecEnd:
        print "DestinationFunction: Problem with number of DOF"
        print "len(OrigDof) =", numpy.shape(OrigDof), "but VecEnd =", VecEnd
        exit(1)
    LocValues = numpy.zeros(dim*VecEnd)
    for LID in range(VecEnd):
        IndexOrig = OrigDof[0][LID]
        for j in range(dim):
            value = fSource[dim*IndexOrig+j]
            LocValues[dim*LID+j] = value
            if j == plane and OrigDof[1][LID] > 0:
                LocValues[dim*LID+j] = -value
    fDestination.vector().set_local(LocValues)
    fDestination.vector().apply("")
    fDestination.rename(MyName, "dummy")
    return fDestination

#OrigDof is mapping from local destination dof to global source dof
#Pull function from DestinationMesh to SourceMesh
#WARNING: Problems with DG > 0
def PullInterMesh(SourceMesh, DestinationMesh, OrigDof, fOrig, Type, Deg, plane=-1):
    dim = fOrig.function_space().element().value_dimension(0)
    VecEnd = fOrig.vector().local_size()/dim
    if dim == 1:
        SourceSpace = FunctionSpace(SourceMesh, Type, Deg)
    else:
        SourceSpace = VectorFunctionSpace(SourceMesh, Type, Deg, dim)
    fSource = Function(SourceSpace)
        
    if not numpy.shape(OrigDof)[1] == VecEnd:
        print "DestinationFunction: Problem with number of DOF"
        print "len(OrigDof) =", numpy.shape(OrigDof), "but VecEnd =", VecEnd
        exit(1)
    GlobalValues = numpy.zeros(len(fSource.vector()))
    for LID in range(VecEnd):
        IndexOrig = OrigDof[0][LID]
        for j in range(dim):
            value = fOrig.vector()[dim*LID+j]
            GlobalValues[dim*IndexOrig+j] = value
            if j == plane and OrigDof[1][LID] > 0:
                GlobalValues[dim*IndexOrig+j] = -value
    SyncSum(GlobalValues)
    LocValues = numpy.zeros(fSource.vector().local_size())
    for i in range(fSource.vector().local_size()):
        GID = SourceSpace.dofmap().local_to_global_index(i)
        LocValues[i] = GlobalValues[GID]
    fSource.vector().set_local(LocValues)
    fSource.vector().apply("")
    fSource.rename(fOrig.name(), "dummy")
    return fSource

#insert value from the copy cell, not the mirror cell
def ReduceFunctionFromMirror(CopyDof, MirrorDof, f, fMirror, dim):
    VecEnd = f.vector().local_size()/dim
    if not len(CopyDof) == VecEnd:
        print "ReduceFunctionFromMirror: Problem with number of DOF1"
        print "Length of CopyDof:", len(CopyDof)
        print "Length of vector:", VecEnd
        exit()
    LocValues = numpy.zeros(dim*VecEnd)
    for i in range(VecEnd):
        IndexCopy = CopyDof[i]
        if IndexCopy >-1:
            for j in range(dim):
                value = fMirror.vector()[dim*IndexCopy+j]
                LocValues[dim*i+j] = value
    f.vector().set_local(LocValues)
    f.vector().apply("")

def ReduceFunction(mesh, SymmetricMesh, CopyDof, MirrorDof, f, Type, Deg, dim):
    if dim == 1:
        OriginalSpace = FunctionSpace(mesh, Type, Deg)
    else:
        OriginalSpace = VectorFunctionSpace(mesh, Type, Deg)
    fCopy = Function(OriginalSpace)
    ReduceFunctionFromMirror(CopyDof, MirrorDof, fCopy, f, dim)
    return fCopy

def ReduceSurfaceFunction(mesh, SymmetricMesh, CopyDof, MirrorDof, f, Type, Deg, dim):
    gamma = BoundaryMesh(mesh, "exterior")
    if dim == 1:
        OriginalSpace = FunctionSpace(gamma, Type, Deg)
    else:
        OriginalSpace = VectorFunctionSpace(gamma, Type, Deg, dim)
    fCopy = Function(OriginalSpace)
    ReduceFunctionFromMirror(CopyDof, MirrorDof, fCopy, f, dim)
    return fCopy

#FEniCS sometimes shows weird behavior when interpolating a surface function into the volume
#In particular the case of missing values after the ComponentLaplaceDefo
#input surface function V, output same function on surface but zeros in the volume
#return the dofs on the mirror mesh based on the dofs of the original mesh
#Warning: THIS COULD THEORETICAL FAIL ON VERY SPECIAL MESHES...
def MakeSurfaceToVolumeDofMapOLD(mesh, gamma, SpaceType, SpaceOrder):
    TestFunctionSpaceV = FunctionSpace(mesh, SpaceType, SpaceOrder)
    TestFunctionSpaceS = FunctionSpace(gamma, SpaceType, SpaceOrder)
    TestExpression = TestExpression
    TestXS = interpolate(TestExpression, TestFunctionSpaceS)
    TestXV = interpolate(TestExpression, TestFunctionSpaceV)
    SurfaceToVolumeDof = numpy.empty(len(TestXS.vector()), dtype=int)
    SurfaceToVolumeDof.fill(-1)
    for i in range(len(TestXS.vector())):
        value1 = TestXS.vector()[i]
        for j in range(len(TestXV.vector())):
            value2 = TestXV.vector()[j]
            if value1 == value2:
                SurfaceToVolumeDof[i] = j
                break
    return SurfaceToVolumeDof

#new version but only for CG1
#does not work in parallel
def MakeSurfaceToVolumeDofMap(mesh, gamma, SpaceType, SpaceOrder):
    if not SpaceType == "CG":
        print "ONLY WORKS FOR CG1 FUNCTIONS"
        exit()
    if not SpaceOrder == 1:
        print "ONLY WORKS FOR CG1 FUNCTIONS"
        exit()
    MoveSpaceV = FunctionSpace(mesh, SpaceType, SpaceOrder)
    MoveSpaceS = FunctionSpace(gamma, SpaceType, SpaceOrder)
    #surface dof to vertex
    SDtoV = dof_to_vertex_map(MoveSpaceS)
    #volume vertex to dof
    VVtoD = vertex_to_dof_map(MoveSpaceV)
    SurfaceToVolumeDof = numpy.empty(len(SDtoV), dtype=int)
    mapa = gamma.entity_map(0)
    #for every local surface dof i
    for i in range(len(SDtoV)):
        #get local ID of surface vertex for ith surface dof
        SVertex = int(SDtoV[i])
        #transform to volume ID for same vertex
        VVertex = mapa[SVertex]
        #get volume dof for this volume vertex
        VDof = VVtoD[VVertex]
        #set value
        SurfaceToVolumeDof[i] = VDof
    return SurfaceToVolumeDof
    
def ReverseBoundaryMap(mesh, gamma):
    mapa = gamma.entity_map(0)
    ReverseMap = MeshFunction('int',mesh,0)
    for i in range(len(ReverseMap.array())):
        ReverseMap[i] = -1
    for i in range(len(mapa.array())):
        ReverseMap[mapa[i]] = i
    return ReverseMap

def ExtentVectorSurfaceFunctionToVolume(mesh, V, boundary_parts = [], SymmX = [], SymmY = [], SymmZ = []):
    gamma = BoundaryMesh(mesh, "exterior")
    MaxDim = mesh.topology().dim()
    SpaceType = "CG"
    SpaceOrder = 1
    #Globalize and order V by global indices
    VGlobal = numpy.zeros(len(V.vector()))
    SurfVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
    (sdmin, sdmax) = V.vector().local_range()
    sdmin = int(sdmin/MaxDim)
    sdmax = int(sdmax/MaxDim)
    for i in range(sdmax-sdmin):
        Vert = MeshEntity(gamma, 0, SurfVert[i])
        IndexGlobal = Vert.global_index()
        for j in range(MaxDim):
            value = V.vector()[MaxDim*(i+sdmin)+j]
            VGlobal[MaxDim*IndexGlobal+j] = value
    VGlobal = SyncSum(VGlobal)
    MyReverseMap = ReverseBoundaryMap(mesh, gamma)
    mapa = gamma.entity_map(0)
    SurfDof = vertex_to_dof_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
    VolDof = vertex_to_dof_map(FunctionSpace(mesh, SpaceType, SpaceOrder))
    VolVert = dof_to_vertex_map(FunctionSpace(mesh, SpaceType, SpaceOrder))
    SpaceV = VectorFunctionSpace(mesh, SpaceType, SpaceOrder, MaxDim)
    VVolume = Function(SpaceV)
    (vdmin, vdmax) = VVolume.vector().local_range()
    vdmin = int(vdmin/MaxDim)
    vdmax = int(vdmax/MaxDim)
    MyData = numpy.zeros(MaxDim*(vdmax-vdmin))
    MyRange = FunctionSpace(mesh, SpaceType, SpaceOrder).dofmap().ownership_range()
    for i in range(vdmin, vdmax):
        GlobalDof = i
        LocalDof = i-vdmin
        LocVert = int(VolVert[LocalDof])
        SurfVert = MyReverseMap[LocVert]
        boundary = 0.0
        if SurfVert != -1:
            Vert = MeshEntity(gamma, 0, SurfVert)
            SDof = SurfDof[SurfVert]
            GlobalIndex = Vert.global_index()
            #if MyCorrectBoundary[GlobalIndex] > 0.0:
            for j in range(MaxDim):
                value = VGlobal[MaxDim*GlobalIndex+j]
                MyData[MaxDim*LocalDof + j] = value
    VVolume.vector().set_local(MyData)
    VVolume.vector().apply('')
    if SymmX != [] or SymmY != [] or SymmZ != []:
        VVolume = FixSymmetryPlane(boundary_parts, VVolume, SymmX, SymmY, SymmZ)
    return VVolume
