#Old routines dealing with meshes and symmetry

#Returns a DG 0 surface vector function with the face normal
def SurfaceFaceNormal(mesh, reorient=True):
    gamma = BoundaryMesh(mesh, "exterior")
    dim = mesh.topology().dim()
    NSpace = VectorFunctionSpace(gamma, 'DG', 0, dim)
    N = Function(NSpace)
    dof_min, dof_max = N.vector().local_range()
    ll = dof_max - dof_min
    values = numpy.empty(ll)
    GlobalValues = numpy.zeros(len(N.vector()))
    for MyFace in entities(gamma, dim-1):
        FaceIndex = MyFace.index()
        MyVertex = MyFace.entities(0)
        ver0 = numpy.zeros(dim)
        ver1 = numpy.zeros(dim)
        ver2 = numpy.zeros(dim)
        if dim == 3:
            for i in range(0,3):
                ver0[i] = Vertex(gamma, MyVertex[0]).x(i)
                ver1[i] = Vertex(gamma, MyVertex[1]).x(i)
                ver2[i] = Vertex(gamma, MyVertex[2]).x(i)
            #1st tangent
            u = [ver1[0]-ver0[0], ver1[1]-ver0[1], ver1[2]-ver0[2]]
            #2nd tangent
            v = [ver2[0]-ver0[0], ver2[1]-ver0[1], ver2[2]-ver0[2]]
            #normal
            n = [u[1]*v[2] - u[2]*v[1], u[2]*v[0] - u[0]*v[2], u[0]*v[1] - u[1]*v[0]]
        else:
            for i in range(0,2):
                ver0[i] = Vertex(gamma, MyVertex[0]).x(i)
                ver1[i] = Vertex(gamma, MyVertex[1]).x(i)
            #1st tangent
            u = [ver1[0]-ver0[0], ver1[1]-ver0[1]]
            #normal
            n = [ver1[1]-ver0[1], -(ver1[0]-ver0[0])]
        norm = numpy.linalg.norm(n)
        n = n/norm
        #check orientation
        if reorient==True:
            mapa = gamma.entity_map(dim-1)
            ParentFace = Facet(mesh, mapa[FaceIndex])
            PTetrahedron = ParentFace.entities(dim)
            PTetrahedron = Cell(mesh, PTetrahedron[0])
            PMid = [0.0]*dim
            for i in PTetrahedron.entities(0):
                Mid = Vertex(mesh, i)
                for j in range(dim):
                    PMid[j] = PMid[j] + (Mid.x(j)/(dim+1))
            PRef = numpy.zeros(dim)
            for i in range(dim):
                PRef[i] = PMid[i]-ver0[i]
            d = 0.0
            for i in range(dim):
                d += n[i]*PRef[i]
            if d > 0.0 and reorient==True:
                n = -n
        IndexGlobal = MyFace.global_index()
        for j in range(dim):
            GlobalValues[dim*IndexGlobal + j] = n[j]
    SAD_MPI.SyncSum(GlobalValues)
    for i in range(dof_min, dof_max):
        values[i-dof_min] = GlobalValues[i]
    N.vector().set_local(values)
    N.vector().apply('')
    N.rename("n", "")
    return N

def NormalFromSignedDistance(SignedDistance, mesh, boundary_parts=None, InteriorIDs=None):
    VSpace = VectorFunctionSpace(mesh, "CG", 1)
    v = TestFunction(VSpace)
    w = TrialFunction(VSpace)
    n = Function(VSpace)
    n.rename("normal", "")
    a = inner(v,w)*(dx+ds)
    l = inner(v,-grad(SignedDistance))*ds
    if InteriorIDs != None:
        dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
        for i in InteriorIDs:
            a += avg(inner(v,w))*dS(i)
            l += avg(inner(v,-grad(SignedDistance)))*dS(i)
    solve(a==l, n)
    return n

#Average a per face vector field to a vertex CG1 vector field
def AverageFaceQuantity(FaceNormal, boundary_parts = [], markers = []):
    NSpace = FaceNormal.function_space()
    gamma = NSpace.mesh()
    MaxDim = NSpace.element().value_dimension(0)
    DofMapFace = NSpace.dofmap()
    #local storage for three vector components per vertex of the whole mesh
    LocValues = numpy.zeros(len(FaceNormal.vector()))
    LocNumAverages = numpy.zeros(len(FaceNormal.vector())/MaxDim)
    GlobalFaceNormal = GlobalizeFunction(FaceNormal)
    for v in vertices(gamma):
        Index = v.index()
        #PARALLEL: Turn local mesh index into global mesh index
        #IndexGlobal = GT.global_indices(0)[Index]
        IndexGlobal = v.global_index()
        Faces = cells(v)#v.entities(MaxDim-1)
        FNormalSum = numpy.zeros(MaxDim)
        MyNumFaces = 0.0
        for f in Faces:
            i = f.index()
            GID = f.global_index()
            if markers != []:
                FaceMarker = boundary_parts.array()[i]
                IsInMarker = FaceMarker in markers
            else:
                IsInMarker = True
            if IsInMarker:
                MyNumFaces = MyNumFaces + 1.0
                FaceDofs = DofMapFace.cell_dofs(i)#Local-to-global mapping of dofs on a cell
                for j in range(MaxDim):
                    ID1 = DofMapFace.local_to_global_index((FaceDofs[j]))
                    value = GlobalFaceNormal[ID1]#FaceNormal.vector()[ID1]
                    FNormalSum[j] = FNormalSum[j] + value
        #LocNumAverages[IndexGlobal] = len(Faces)
        LocNumAverages[IndexGlobal] = MyNumFaces
        for j in range(MaxDim):
            LocValues[MaxDim*IndexGlobal + j] = FNormalSum[j]
    LocValues = SyncSum(LocValues)
    LocNumAverages = SyncSum(LocNumAverages)
    #from now on, LocValues actually contains the globally summed face normals into vertex normals
    MoveSpaceS = FunctionSpace(gamma, "CG", 1)
    MoveSpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
    VertexNormal = Function(MoveSpaceV)
    dof_min, dof_max = VertexNormal.vector().local_range()
    dof_max = int(dof_max/MaxDim)
    dof_min = int(dof_min/MaxDim)
    ll = dof_max - dof_min
    MyValues = numpy.empty(MaxDim*ll)
    for i in range(ll):
        GDof = i#dof_min + i
        vert = dof_to_vertex_map(MoveSpaceS)[GDof]
        MyVert = MeshEntity(gamma, 0, vert)
        IndexGlobal = MyVert.global_index()
        #IndexGlobal = GT.global_indices(0)[vert]
        quotient = LocNumAverages[IndexGlobal]
        if quotient > DOLFIN_EPS:
            norm = 0.0
            for j in range(MaxDim):
                NormalJ = LocValues[MaxDim*IndexGlobal + j]/quotient
                MyValues[MaxDim*i+j] = NormalJ
                norm = norm + NormalJ*NormalJ
            norm = sqrt(norm)
            for j in range(MaxDim):
                MyValues[MaxDim*i+j] = MyValues[MaxDim*i+j]/norm
    VertexNormal.vector().set_local(MyValues)
    VertexNormal.vector().apply('')
    VertexNormal.rename(FaceNormal.name(), "dummy")
    return VertexNormal

#Only works for 3D Vectorfield of type "CG" 1
#and only for a vectorfield over the internal mesh
def NormalizeVectorField(V, fixed=[]):
    mesh = V.function_space().mesh()
    coords = mesh.coordinates()
    MaxDim = V.function_space().element().value_dimension(0)
    d2v = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
    NewValues = numpy.zeros(V.vector().local_size())
    for i in range(V.vector().local_size()/MaxDim):
        x = numpy.array(coords[d2v[i]])
        norm = 0.0
        for j in range(MaxDim):
            if not (j in fixed and near(x[j], 0.0, TestTol)):
                value = V.vector()[MaxDim*i + j]
                norm = norm + value*value
        norm = sqrt(float(norm))
        #do not normalize if quantity is zero...
        if norm > DOLFIN_EPS:
            for j in range(MaxDim):
                if j in fixed and near(x[j], 0.0, TestTol):
                    NewValues[MaxDim*i + j] = 0.0
                else:
                    NewValues[MaxDim*i + j] = V.vector()[MaxDim*i + j]/norm
    V.vector().set_local(NewValues)
    V.vector().apply('')

#Take a CG1 Vector Function, aka the normal on a subsurface, and make
#it zero except for precisely one node of global index GID
#and scale with MyEps
#Helps with finite differences...
def ReduceVectorFieldToNode(V, GID, MyEps = 1.0):
    mesh = V.function_space().mesh()
    MaxDim = V.function_space().element().value_dimension(0)
    LocSize = V.vector().local_size()
    LocValues = numpy.zeros(LocSize)
    V2D = vertex_to_dof_map(FunctionSpace(mesh, "CG", 1))
    V_new = Function(VectorFunctionSpace(mesh, "CG", 1, MaxDim))
    for v in vertices(mesh):
        if v.global_index() == GID:
            MyDof = V2D[v.index()]
            for i in range(MaxDim):
                LocValues[MaxDim*MyDof+i] = V.vector()[MaxDim*MyDof+i]
    V_new.vector().set_local(LocValues)
    V_new.vector().apply("")
    V_new.rename("n", "dummy")
    return V_new

def MakeV(mesh, boundary_parts):
    gamma = BoundaryMesh(mesh, "exterior")
    V = SurfaceFaceNormal(mesh)
    V = AverageFaceQuantity(V)
    #fix x-movement
    #all degrees of freedom
    NormalizeVectorField(V)
    V = InjectOnSubSurface(V, mesh, boundary_parts, VariableBoundary)
    V.rename("V", "Dummy")
    return V

#returns surface vector...
def MultiplySurfaceWithVn(SGrad, StepLength, V, mesh, boundary_parts):
    if not MaxOrder == 1:
        print "ERROR multiply (V,n) only works for 1st order"
        exit()
    MaxDim = mesh.topology().dim()
    gamma = BoundaryMesh(mesh, "exterior")
    SpaceS = FunctionSpace(gamma, "CG", 1)
    SpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
    SGradV = Function(SpaceV)
            
    dof_min, dof_max = SGradV.vector().local_range()
    dof_min = int(dof_min/MaxDim)
    dof_max = int(dof_max/MaxDim)
    ll = dof_max - dof_min
    LocValues = numpy.zeros(MaxDim*ll)

    N = SurfaceFaceNormal(mesh)
    N = AverageFaceQuantity(N)
    NormalizeVectorField(N)
    
    #exclude boundary
    gamma.init()
    mapa = gamma.entity_map(MaxDim-1)
    mesh.init()
    LocalToGlobal = gamma.topology().global_indices(0)
    MyCorrectBoundary = VertexOnBoundary(mesh, boundary_parts, VariableBoundary)

    DofToVertex = dof_to_vertex_map(SpaceS) #maps to local/processor dofs
    for i in range(dof_min, dof_max):
        VertexID = DofToVertex[i-dof_min]
        Vert = MeshEntity(gamma, 0, VertexID)
        GlobalIndex = Vert.global_index()
        if MyCorrectBoundary[GlobalIndex] > 0.0:
            GradValue = SGrad.vector()[i]
            factor = 0.0
            for j in range(0, MaxDim):
                LocN = N.vector()[MaxDim*i+j]
                LocV = V.vector()[MaxDim*i+j]
                factor = factor + StepLength*LocN*LocV
            for j in range(0, MaxDim):
                LocV = V.vector()[MaxDim*i+j]
                LocValues[MaxDim*(i-dof_min)+j] = factor*GradValue*LocV
    SGradV.vector().set_local(LocValues)
    SGradV.vector().apply('')
    return SGradV

def MakeV_old(mesh, boundary_parts):
    gamma = BoundaryMesh(mesh, "exterior")
    dss=ds[boundary_parts]
    N = VectorFunctionSpace(mesh, 'CG', 1)
    n = FacetNormal(mesh)
    v = TestFunctions(N)
    w = TrialFunctions(N)
    a = (inner(v[0],w[0]) + inner(v[1],w[1]) + inner(v[2],w[2]))*dx
    a += (inner(v[0],w[0]) + inner(v[1],w[1]) + inner(v[2],w[2]))*dss
    l = (inner(n[0],v[0]) + inner(n[1],v[1]) + inner(n[2],v[2]))*(dss(1)+dss(4))
    V = Function(N)

    def Boundary1(x):
        return abs(x[2]) < 1e-8#DOLFIN_EPS
    def Boundary2(x):
        return abs(x[1]) < 1e-8#DOLFIN_EPS

    B1  = DirichletBC(N.sub(2), 0.0, Boundary1)
    B2  = DirichletBC(N.sub(1), 0.0, Boundary2)
    solve(a==l, V)

    NN = VectorFunctionSpace(gamma, 'CG', 1)
    V = interpolate(V, NN)
        
#Average normals
def vertex_normal(mesh, boundary_parts, i, label):
    ver = Vertex(mesh, i)
    n = Point(0, 0, 0)
    div = 0.0
    for fac in entities(ver, 2):
        f = Facet(mesh, fac.index())
        if f.exterior()==True:
            n+=f.normal()
            div+=1
    if div !=0:
        n/=div
    return n

#omega=mesh
#mf =boundary parts marker
#Computes Vertex Normals
def compute_normal_field(omega, mf):
    #Boundary Mesh
    gamma = BoundaryMesh(omega, "exterior")
    mapa = gamma.entity_map(0)
    V = FunctionSpace(gamma, "CG", 1)
    N = VectorFunctionSpace(gamma, "CG", 1)
    normal_field = Function(N)

    normal_x = Function(V)
    normal_y = Function(V)
    normal_z = Function(V)

    for ver in entities(gamma, 0):
        i = mapa[ver.index()]
        point = vertex_normal(omega, mf, i, ([1]))
        normal_x.vector()[ver.index()] = -1.0*point[0]
        normal_y.vector()[ver.index()] = -1.0*point[1]
        normal_z.vector()[ver.index()] = -1.0*point[2]

    (x, y, z) = TestFunctions(N)
    solve(inner(normal_field[0], x)*dx + inner(normal_field[1], y)*dx + inner(normal_field[2], z)*dx -\
    inner(normal_x, x)*dx -inner(normal_y, y)*dx -inner(normal_z, z)*dx == 0, normal_field)

    #Dirichlet Boundary must be initialized by Function in Vector Space over Omega (only on Gamma fails)
    V_vec = VectorFunctionSpace(omega, "CG", 1)
    normal_fieldV = Function(V_vec)
    for ver in entities(gamma, 0):
        i = mapa[ver.index()]
        normal_fieldV.vector()[i] = normal_field.vector()[ver.index()]
        normal_fieldV.vector()[i+omega.num_vertices()] = normal_field.vector()[ver.index()+gamma.num_vertices()]
        normal_fieldV.vector()[i+2*omega.num_vertices()] = normal_field.vector()[ver.index()+2*gamma.num_vertices()]
    
    deform= project(normal_fieldV,V_vec)
    return deform

def compute_subsurface_vertex_normal(omega, boundary_parts, marker,nbb):
    #Extract Subsurface with ID marker
    gamma = BoundaryMesh(omega, "exterior")
    mapa = gamma.entity_map(2)
    boundaryboundary_parts=MeshFunction('size_t',gamma,2)
    for ver in entities(gamma, 2):
        i = mapa[ver.index()]
        boundaryboundary_parts[ver.index()] = boundary_parts[i]
    gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)

    #Construct face normales for all boundaries
    NB = VectorFunctionSpace(gamma, 'DG', 0, 3)
    nb = Function(NB)
    NumFacets = gamma.num_entities(2)
    for ver in entities(gamma,2):
        k = mapa[ver.index()]
        fac = Facet(omega, k)
        n_fac = fac.normal()
        nb.vector()[ver.index()] = -1.0*n_fac[0]
        nb.vector()[ver.index()+NumFacets] = -1.0*n_fac[1]
        nb.vector()[ver.index()+2*NumFacets] = -1.0*n_fac[2]

    #Reduce face normals from all boundaries to boundary labeled "marker"
    NBB = VectorFunctionSpace(gammagamma, 'DG', 0, 3)
    mapb = gammagamma.data().mesh_function("parent_cell_indices")

    for ver in entities(gammagamma, 2):
        i=mapb[ver.index()]
        nbb.vector()[ver.index()] = nb.vector()[i]
        nbb.vector()[ver.index()+gammagamma.num_entities(2)] = nb.vector()[i+gamma.num_entities(2)]
        nbb.vector()[ver.index()+2*gammagamma.num_entities(2)] = nb.vector()[i+2*gamma.num_entities(2)]
    return nbb

def VisualizeMeshH5(self, FileName, mesh):
    FSpace = FunctionSpace(mesh, "DG", 0)
    Z = project(Constant(0.0), FSpace)
    MyFile = XDMFFile(mpi_comm_world(), FileName)
    MyFile << mesh

def FixMeshFunctionParallel(self, mf):
    Max = mf.array().max()
    Min = mf.array().min()
    ErrorLow = -1
    ErrorHigh = 1000
    if Max > ErrorHigh or Min < ErrorLow:
        mf.mesh().init()
        for i in range(len(mf.array())):
            value = mf.array()[i]
            if value > ErrorHigh or value < ErrorLow:
                Ent = Facet(mf.mesh(), i)
                if(Ent.exterior() == False):
                    mf.array()[i] = 0
                else:
                    nvalues = mf.array()[Ent.entities(mf.dim())]
                    GoodValues = []
                    for j in nvalues:
                        if j > ErrorLow and j < ErrorHigh and j != 0:
                            GoodValues.append(j)
                    if len(GoodValues) > 0:
                        def most_common(L):
                            import itertools
                            groups = itertools.groupby(sorted(L))
                            def _auxfun((item, iterable)):
                                return len(list(iterable)), -L.index(item)
                            return max(groups, key=_auxfun)[0]
                        OkValue = most_common(GoodValues)
                        mf.array()[i] = OkValue
                    else:
                        mf.array()[i] = 0
