#Old Routines dealing with basic arrays

def AddVectors(self, V1, V2, f1=1.0, f2=1.0):
    FunctionSpace = V1.function_space()
    Sum = Function(FunctionSpace)
        
    aa = Sum.vector().local_size()
    (a,b) = Sum.vector().local_range()
        
    #Sanity check:
    (a1,b1) = V1.vector().local_range()
    (a2,b2) = V2.vector().local_range()
    if not (a == a1 and a1 == a2 and b == b1 and b1 == b2):
        self.dprint("Error using AddVectors. Data locality no equal")
        self.dprint("a %d, b %d"%(a,b))
        self.dprint("a1 %d, b1 %d, a2 %d, b2 %d"%(a1, b1, a2, b2))
        exit()
    LocValues = numpy.zeros(aa)
    #Modification for Dolfin 1.5 (with Ghost Layers)
    for i in range(aa):
        LocValues[i] = f1*V1.vector()[i]+f2*V2.vector()[i]
    Sum.vector().set_local(LocValues)
    Sum.vector().apply("")
    Sum.rename("Sum", "Dummy")
    return Sum

def RescaleVector(self, MyScalar, MyFunction, MyVector):
    MaxDim = MyVector.function_space().element().value_dimension(0)
    if MyFunction.vector().local_size()*MaxDim != MyVector.vector().local_size():
        self.dprint("ERROR: RescaleVector: SIZE MISMATCH")
        exit(1)
    MySize = MyFunction.vector().local_size()
    LocValues = numpy.zeros(MaxDim*MySize)
    for i in range(MySize):
        value = MyScalar*MyFunction.vector()[i]
        for j in range(MaxDim):
            LocValues[MaxDim*i+j] = value*MyVector.vector()[MaxDim*i+j]
    MyMesh = MyVector.function_space().mesh()
    MyVector2 = Function(VectorFunctionSpace(MyMesh, "CG", 1, MaxDim))
    MyVector2.vector().set_local(LocValues)
    MyVector2.vector().apply("")
    MyVector2.rename(MyVector.name(), "dummy")
    return MyVector2

def CalculateScalarL2NormBoundary(self, SurGrad, mesh, boundary_parts):
    (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary, mesh, boundary_parts)
    SmoothSpace = FunctionSpace(VariableSurface, "CG", self.MaxOrder)

    SurGrad = interpolate(SurGrad, SmoothSpace)

    a = (SurGrad*SurGrad)*dx
    a = assemble(a)
    return sqrt(a)

def CalculateVectorL2NormBoundary(self, SGV, mesh, boundary_parts):
    (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary, mesh, boundary_parts)
    SmoothSpace = VectorFunctionSpace(VariableSurface, "CG", self.MaxOrder)

    SGV = interpolate(SGV, SmoothSpace)
    a = (SGV[0]*SGV[0]+SGV[1]*SGV[1]+SGV[2]*SGV[2])*dx
    a = assemble(a)
    return sqrt(a)

def SplitVectorFunction(h, omega, degree):
    H = VectorFunctionSpace(omega, 'DG', degree)
    H0 = FunctionSpace(omega, 'DG', degree)
    h0 = Function(H0)
    h1 = Function(H0)
    h2 = Function(H0)
    x = TestFunction(H0)
    solve(inner(h[0], x)*dx - inner(h0, x)*dx == 0, h0)
    h0 = project(h0, H0)
    solve(inner(h[1], x)*dx - inner(h1, x)*dx == 0, h1)
    h1 = project(h1, H0)
    solve(inner(h[2], x)*dx - inner(h2, x)*dx == 0, h2)
    h2 = project(h2, H0)
    return h0, h1, h2

def CombineVectorFunction(h0, h1, h2, omega, degree):
    N = VectorFunctionSpace(omega, "DG", degree)
    H = Function(N)
    (x, y, z) = TestFunctions(N)
    solve(inner(H[0], x)*dx + inner(H[1], y)*dx + inner(H[2], z)*dx -\
    inner(h0, x)*dx -inner(h1, y)*dx -inner(h2, z)*dx == 0, H)
    return H

def StoreAppendFunction(self, MyFolder, MyIter, MyFunction):
    FName = MyFunction.name()
    if FName not in self.VisFunctions:
        MyXFile = XDMFFile(self.Communicator, MyFolder+"/"+FName+".xdmf")
        MyXFile.parameters["rewrite_function_mesh"] = self.ReWriteMesh
        MyXFile.parameters["flush_output"] = self.FlushOutput
        self.VisFunctions.update({FName:MyXFile})
        self.VisFunctions[FName] << (MyFunction, float(MyIter))
        #update restart file of stored functions:
        if MPI.rank(mpi_comm_world()) == 0 and self.Restart == False:
            NameFile = open(MyFolder+"/VisFileNames.txt", "a")
            NameFile.write(FName + " " + str(MyFunction.function_space().element().value_dimension(0))+" "+MyFunction.function_space().element().signature()+"\n")
                NameFile.close()
    else:
        self.VisFunctions[FName] << (MyFunction, float(MyIter))
    MyFile = HDF5File(mpi_comm_world(), MyFolder+"/RAW/"+FName+"_"+str(MyIter)+".h5", "w")
    MyFile.write(MyFunction, FName)
    MyFile.close()

#Recreate the morph visualization after a restart
def ReconstructVisualization(self, MyFolder, MyIter):
    NameFile = open(MyFolder+"/RAW/VisFileNames.txt", "r")
    for line in NameFile:
        MyLine = line.split()
        FName = MyLine[0]
        MaxDim = int(MyLine[1])
        self.pprint("Reconstructing "+FName)
        #Get the FEniCS Element Type Descriptor
        FEIdentifier = MyLine[2]
        MeshDim = 3
        start = FEIdentifier.find("'")+1
        end = start+FEIdentifier[start:].find("'")
        FEType = FEIdentifier[start:end]
        FECell = MyLine[3]
        if "triangle" in FECell:
            OrderIndex = 5
            IsSurf= True
        else:
            OrderIndex = 4
            IsSurf = False
        FEOrder = MyLine[OrderIndex]
        FEOrder = MyLine[OrderIndex][MyLine[OrderIndex].rfind(")")-1]
        FEOrder = int(FEOrder)
        self.pprint("Reconstructing first %d iterations of Function "%MyIter+FName+" of type "+FEType+" of order %d %d"%(FEOrder, MaxDim))
        MyXFile = XDMFFile(self.Communicator, MyFolder+"/"+FName+".xdmf")
        MyXFile.parameters["rewrite_function_mesh"] = True
        MyXFile.parameters["flush_output"] = True
        self.VisFunctions.update({FName:MyXFile})
        for i in range(MyIter):
            MyName = MyFolder+"/../"+"OptIter%d"%i
            if IsSurf == False:
                (TMesh, TBP) = self.LoadMeshH5(MyName+"/Mesh.h5")
                if MaxDim > 0:
                    MySpace = VectorFunctionSpace(TMesh, FEType, FEOrder, int(MaxDim))
                else:
                    MySpace = FunctionSpace(TMesh, FEType, FEOrder)
                MyF = Function(MySpace)
                MyFile = HDF5File(mpi_comm_world(), MyFolder+"/RAW/"+FName+"_"+str(i)+".h5", "r")
                MyFile.read(MyF, FName)
                MyFile.close()
                MyF.rename(FName, "dummy")
                self.VisFunctions[FName] << (MyF, float(i))
    NameFile.close()
