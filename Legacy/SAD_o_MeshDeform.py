#Old routines for mesh deformation and repair

def FixQuadrant(mesh, MaxDim, Quadrants, tol = 2e-3, MinDist = 1.25e-2, InitField = []):
    SpaceV = VectorFunctionSpace(mesh, "CG", 1, MaxDim)
    SpaceS = FunctionSpace(mesh, "CG", 1)
    Field = Function(SpaceV)
    (dmin, dmax) = Field.vector().local_range()
    dmin = int(dmin/MaxDim)
    dmax = int(dmax/MaxDim)
    LocValues = numpy.zeros(MaxDim*(dmax-dmin))
    DtoV = dof_to_vertex_map(SpaceS)
    for i in range(dmin, dmax):
        v = Vertex(mesh, DtoV[i-dmin])
        for k in Quadrants:
            x_k = v.x(k)
            #tolerace when to accept a point that is on the plane anyway
            #and should stay there...
            if abs(x_k) > tol:
                if InitField != []:
                    x_knew = x_k + InitField.vector()[MaxDim*i + k]
                else:
                    x_knew = x_k
                #check if x_knew is too close to the boundary or has transgressed already:
                TooFar = 0.0
                if (abs(x_knew) < MinDist):
                    TooFar = MinDist - abs(x_knew)
                elif (math.copysign(1.0, x_knew) != math.copysign(1.0, x_k)):
                    TooFar = abs(x_knew) + MinDist
                LocValues[MaxDim*(i-dmin) + k] = math.copysign(TooFar, x_k)
            #force point perfectly onto symmetry
            else:
                LocValues[MaxDim*(i-dmin) + k] = -1.0*x_k
    Field.vector().set_local(LocValues)
    Field.vector().apply("")
    return Field
    
def FixSymmetryPlane(boundary_parts, VectorFunction, XIDs = [], YIDs = [], ZIDs = []):
    Space = VectorFunction.function_space()
    for i in XIDs:
        BC = DirichletBC(Space.sub(0), Constant(0.0), boundary_parts, i)
        BC.apply(VectorFunction.vector())
    for i in YIDs:
        BC = DirichletBC(Space.sub(1), Constant(0.0), boundary_parts, i)
        BC.apply(VectorFunction.vector())
    for i in ZIDs:
        BC = DirichletBC(Space.sub(2), Constant(0.0), boundary_parts, i)
        BC.apply(VectorFunction.vector())
    return VectorFunction

def BLaplaceSmooth(SurGrad, boundary_parts, epsilon, NumSmooth, closed=False):
    mesh = SurGrad.function_space().mesh()
    (VariableSurface, VariableBoundary_parts) = ExtractSubsurface(VariableBoundary, mesh, boundary_parts)
    SmoothSpace = FunctionSpace(VariableSurface, "CG", 1)
        
    gamma = BoundaryMesh(mesh, "exterior")
    u_old = ReduceFunctionToSubsurface(SurGrad, gamma, VariableSurface, "CG", 1)
        
    v = TestFunction(SmoothSpace)
    w = TrialFunction(SmoothSpace)
        
    a = (epsilon*inner(grad(v), grad(w)) + v*w)*dx
    A = assemble(a)
    l = v*u_old*dx
        
    u_0 = Constant(0.0)
    bc = DirichletBC(SmoothSpace, u_0, "on_boundary")
        
    u_next = Function(SmoothSpace)
    for i in range(0, NumSmooth):
        b = assemble(l)
        if closed == False:
            bc.apply(A,b)
        solve(A, u_next.vector(), b)
        u_old.assign(u_next)
    u_old = ExpandFunctionFromSubsurface(u_old, gamma, VariableSurface, "CG", 1)
    return u_old

def BLaplaceSmoothPeriodic(SurGrad, epsilon, NumSmooth):
    class PeriodicBoundary(SubDomain):
        def inside(x, on_boundary):
            #return true if on top or right
            return bool((near(x[1], 0.0) and x[2] > 0.0) or (near(x[2], 0.0) and x[1] > 0.0))
        def map(x, y):
            if near(x[1], 0.0):
                y[0] = x[0]
                y[1] = x[1]
                y[2] = -x[2]
            elif near(x[2], 0.0):
                y[0] = x[0]
                y[1] = -x[1]
                y[2] = x[2]
            #not clear why this is needed but dolfin crashes unless you assign all points.
            #result needs to be outside the boundary...
            else:
                y[0] = -10000
                y[1] = -10000
                y[2] = -10000

    (VariableSurface, VariableBoundary_parts) = ExtractSubsurface(VariableBoundary)
    BoundarySpace = FunctionSpace(VariableSurface, "CG", MaxOrder)
    SmoothSpace = FunctionSpace(VariableSurface, "CG", MaxOrder, constrained_domain=PeriodicBoundary())

    u_old = interpolate(SurGrad, BoundarySpace)

    v = TestFunction(SmoothSpace)
    w = TrialFunction(SmoothSpace)

    a = (epsilon*inner(grad(v), grad(w)) + v*w)*dx
    A = assemble(a)
    l = v*u_old*dx

    u_0 = Constant(0.0)
    bc = DirichletBC(SmoothSpace, u_0, "on_boundary")

    u_next = Function(SmoothSpace)
    for i in range(0, NumSmooth):
        b = assemble(l)
        bc.apply(A,b)
        solve(A, u_next.vector(), b)
        u_old.assign(u_next)

    return u_old

def ComponentLaplaceDefo(mesh, boundary_parts, V, Stiffness = Constant(0.1), closed = False, VariableIDs = [], FixedIDs = [], FixX = [], FixY = [], FixZ = []):
        MaxDim = mesh.topology().dim()
        N = VectorFunctionSpace(mesh, 'CG', 1, MaxDim)
        if V.function_space().mesh().geometry().dim() == mesh.geometry().dim()-1:
            pprint("ComponentLaplaceDefo: Extending Function from Surface to Volume")
            V = ExtentVectorSurfaceFunctionToVolume(mesh, V, boundary_parts, FixX, FixY, FixZ)
        v = TestFunctions(N)
        w = TrialFunctions(N)
        a = 0.0
        l = 0.0
        for i in range(MaxDim):
            a += Stiffness*inner(grad(v[i]),grad(w[i]))*dx
            l += Constant(0.0)*v[i]*dx
        if closed == True:
            for i in range(MaxDim):
                a += v[i]*w[i]*ds
                l += v[i]*V[i]*ds
        A = assemble(a)
        b = assemble(l)
        if MaxDim == 3:
            Zero = (0.0, 0.0, 0.0)
        elif MaxDim == 2:
            Zero = (0.0, 0.0)
        bc = []
        if boundary_parts != []:
            if closed == False:
                for i in VariableIDs:
                    bc.append(DirichletBC(N, V, boundary_parts, i))
                for i in FixedIDs:
                    bc.append(DirichletBC(N, Zero, boundary_parts, i))
                for i in FixX:
                    bc.append(DirichletBC(N.sub(0), 0.0, boundary_parts, i))
                for i in FixY:
                    bc.append(DirichletBC(N.sub(1), 0.0, boundary_parts, i))
                for i in FixZ:
                    bc.append(DirichletBC(N.sub(2), 0.0, boundary_parts, i))
        else:
            bc.append(DirichletBC(N, V, "on_boundary"))
        for MyBC in bc:
            MyBC.apply(A,b)
        if True:
            PreconditionerType = "default"
            solver=KrylovSolver(A, "gmres")
            solver.parameters['nonzero_initial_guess'] = False#True
            solver.parameters['relative_tolerance'] = 1e-8
            solver.parameters['absolute_tolerance'] = 1e-15
            #solver.parameters['preconditioner']['structure'] = 'same'
            solver.parameters['preconditioner']['structure'] = 'different_nonzero_pattern'
            solver.parameters["monitor_convergence"] = False#True
        else:
            solver=LUSolver(A, "default")
            solver.parameters["reuse_factorization"]=False#True
        Sol = Function(N)
        solver.solve(Sol.vector(), b)
        return Sol

"""
    def ConvectionDeformation(V, Diffusion = 1e-2, VariableIDs = [], FixedIDs = []):
    import ConvectionDiffusion as CDS
    MaxDim = mesh.topology().dim()
    Deg = 1
    CDSolver = CDS.ConvectionDiffusion(mesh, boundary_parts, "./CDDeformation")
    b1 = EikonalDistance(VariableIDs, mesh, boundary_parts, 10)
    b2 = EikonalDistance(FixedIDs, mesh, boundary_parts, 10)
    XDMFFile(mpi_comm_world(), "./output/b1.xdmf") << b1
    XDMFFile(mpi_comm_world(), "./output/b2.xdmf") << b2
    CDSolver.Wind = b2*grad(b1)
    CDSolver.HeatConduct = b1*b1*Diffusion#b1*5e-2
    CDSolver.DeltaT = 0.0#1e-2
    CDSolver.NumTimeSteps = 1
    CDSolver.StoreHDD = True
    CDSolver.StoreScalars = False
    CDSolver.DegT = Deg
    CDSolver.StrongBoundaryConditions = True
    Move = Function(VectorFunctionSpace(mesh, "CG", Deg))
    (dmin, dmax) = Move.vector().local_range()
    dmin = int(dmin/MaxDim)
    dmax = int(dmax/MaxDim)
    LocValues = numpy.zeros(MaxDim*(dmax-dmin))
    for j in range(MaxDim):
        CDSolver.BoundaryID = []
        CDSolver.BValue = []
        CDSolver.DWeight = []
        CDSolver.NWeight = []
        for i in VariableIDs:
            CDSolver.BoundaryID.append(i)
            CDSolver.BValue.append(V[j])
            CDSolver.DWeight.append(Constant(1.0))
            CDSolver.NWeight.append(Constant(0.0))
        for i in FixedIDs:
            CDSolver.BoundaryID.append(i)
            CDSolver.BValue.append(Constant(0.0))
            CDSolver.DWeight.append(Constant(1.0))
            CDSolver.NWeight.append(Constant(0.0))
        CDSolver.Solve()
        #Modification for Dolfin 1.5 (with Ghost Layers)
        for i in range(CDSolver.t0.vector().local_size()/MaxDim):
        #for i in range(dmax-dmin):
            #LocValues[MaxDim*i+j] = CDSolver.t0.vector()[i+dmin]
            LocValues[MaxDim*i+j] = CDSolver.t0.vector()[i]
            #Move.vector()[MaxDim*i+j] = CDSolver.t0.vector()[i]
    Move.vector().set_local(LocValues)
    Move.vector().apply("")
    return Move
"""
    
def VolDefo(mesh, boundary_parts, V, Stiffness=0.1, rhs=1.0, MaxBoundary=6):
    Stiffness = Constant(Stiffness)
    N = VectorFunctionSpace(mesh, 'CG', MaxOrder)
    
    v = TestFunctions(N)
    w = TrialFunctions(N)
    a = Stiffness*(inner(grad(v[0]),grad(w[0])) + inner(grad(v[1]),grad(w[1])) + inner(grad(v[2]),grad(w[2])))*dx
    a += (inner(v[0],w[0])+inner(v[1],w[1])+inner(v[2],w[2]))*dx
    l = Constant(rhs)*(V[0]*v[0]+V[1]*v[1]+V[2]*v[2])*dx
    
    Sol = Function(N)
    
    if not boundary_parts == []:
        bc = []
        for i in VariableBoundary:
            bc.append(DirichletBC(N, V, boundary_parts, i))
        for i in range(1, MaxBoundary+1):
            if not i == VariableBoundary:
                bc.append(DirichletBC(N, (0.0,0.0,0.0), boundary_parts, i))
        solve(a==l, Sol, bc)
    else:
        print "ComponentLaplaceDefo: APPLYING BCs Typ 2"
        bc0 = DirichletBC(N, V, "on_boundary")
        solve(a==l, Sol, bc0)
    return Sol

"""
def CurvatureFromSignedDistance(SignedDistance, boundary_parts=None, InteriorEdges=None):
    Space = SignedDistance.function_space()
    w = TrialFunction(Space)
    v = TestFunction(Space)
    kappa = Function(Space)
    kappa.rename("kappa", "")
    a = (inner(v, w)+0.001*inner(grad(v), grad(w)))*(dx+ds)
    l = inner(grad(v), grad(SignedDistance))*(dx+ds)
    
    if boundary_parts != None:
        mesh = Space.mesh()
        dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
        for i in InteriorEdges:
            a += jump((inner(v, w)+0.001*inner(grad(v), grad(w))))*dS(i)
            l += jump(inner(grad(v), grad(SignedDistance)))*dS(i)
    
    solve(a==l,kappa)
    plot(kappa, interactive=True)
    return kappa
"""
    
def DiscreteMeshLaplaceSmooth(mesh, gamma, Codim, lam, mu=0.0, FixedBoundary = [], Weights = [], Tangential = False):
    MaxDim = mesh.topology().dim()
    if Codim == 0:
        MeshToFix = mesh
    if Codim == 1:
        OnCorrectBoundary = VertexOnBoundary(FixedBoundary)
        MeshToFix = gamma
    MoveSpaceV = VectorFunctionSpace(MeshToFix, "CG", 1, MaxDim)
    
    MoveNew = Function(MoveSpaceV)
    SizeGlobal = len(MoveNew.vector())
    (dmin, dmax) = MoveNew.vector().local_range()
    dmin = int(dmin/MaxDim)
    dmax = int(dmax/MaxDim)
    LocValues = numpy.zeros(MaxDim*(dmax-dmin))
    d2v = dof_to_vertex_map(FunctionSpace(MeshToFix, "CG", 1))
    NumNeighbour = numpy.zeros(SizeGlobal)
    if Weights != []:
        if len(Weights.vector()) != SizeGlobal/MaxDim:
            pprint("Not enough weights for every node...")
            exit()
        GlobalWeights = numpy.zeros(int(SizeGlobal/MaxDim))
        for i in range(dmin, dmax):
            GlobalWeights[i] = Weights.vector()[i]
        SyncSum(GlobalWeights)
    Coords = numpy.zeros(MaxDim*SizeGlobal)
    if Tangential == True:
        #face normal
        N = SurfaceFaceNormal(mesh)
        #point normal
        N = AverageFaceQuantity(N)
        NormalizeVectorField(N)
        #Extent to Volume: stay n at boundary and become zero in volume...
        if Codim == 0:
            N = ExtentVectorSurfaceFunctionToVolume(mesh, N)
    #Communicate data
    for v1 in vertices(MeshToFix):
        Index = v1.index()
        IndexGlobal = v1.global_index()
        if Codim == 1:
            if OnCorrectBoundary[IndexGlobal] < 1.0:
                continue
        neighbors = [];
        if Weights != []:
            WeightSum = 0.0
        for e in edges(v1):
            for j in range(2):
                if not e.entities(0)[j] == Index:
                    neighbors.append(e.entities(0)[j])
                    if Weights != []:
                        Vert = Vertex(MeshToFix, e.entities(0)[j])
                        MyIndex = Vert.global_index()
                        WeightSum += (1.0 + GlobalWeights[MyIndex])
        NumConnections = len(neighbors)
        NumNeighbour[IndexGlobal] = NumConnections
        if Weights != []:
            NumNeighbour[IndexGlobal] = WeightSum
        for i in range(NumConnections):
            VPartner = Vertex(MeshToFix, neighbors[i])
            PartnerIndex = VPartner.global_index()
            for j in range(MaxDim):
                x = VPartner.x(j)# + GlobalMove[MaxDim*PartnerIndex+j]
                if Weights != []:
                    x *= (1.0 + GlobalWeights[PartnerIndex])
                Coords[MaxDim*IndexGlobal+j] += x
    NumNeighbour = SyncSum(NumNeighbour)
    Coords = SyncSum(Coords)
    #standard discrete mesh smoothing
    for i in range(0, dmax-dmin):
        v1 = Vertex(MeshToFix, d2v[i])
        Index = v1.index()
        IndexGlobal = v1.global_index()
        if Codim == 1:
            if OnCorrectBoundary[IndexGlobal] < 1.0:
                continue
        #compute projection factor
        if Tangential == True:
            value = 0.0
            for j in range(MaxDim):
                x = v1.x(j)
                value += (Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal] - x)*N.vector()[MaxDim*(i+dmin)+j]
        XNew = [0.0]*MaxDim
        for j in range(MaxDim):
            x = v1.x(j)
            XNew[j] = Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal] - x
            if Tangential == True:
                XNew[j] -= value*N.vector()[MaxDim*(i+dmin)+j]
            LocValues[MaxDim*i + j] += lam*XNew[j]
    #forward/backward smoothing, aka "Taubin" smoothing
    if mu > 0.0:
        for i in range(0, dmax-dmin):
            v1 = Vertex(MeshToFix, d2v[i])
            Index = v1.index()
            IndexGlobal = v1.global_index()
            if Codim == 1:
                if OnCorrectBoundary[IndexGlobal] < 1.0:
                    continue
            #compute projection factor
            if Tangential == True:
                value = 0.0
                for j in range(MaxDim):
                    x = v1.x(j)# + GlobalMove[MaxDim*IndexGlobal + j]
                    value += (Coords[MaxDim*IndexGlobal + j] - NumNeighbour[IndexGlobal]*x)*N.vector()[MaxDim*(i+dmin)+j]
            XNew = [0.0]*MaxDim
            for j in range(MaxDim):
                XNew[j] = Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal]
                if Tangential == True:
                    XNew[j] -= value*N.vector()[MaxDim*(i+dmin)+j]/NumNeighbour[IndexGlobal]
                Xlam = LocValues[MaxDim*i + j]
                x = v1.x(j)# + GlobalMove[MaxDim*IndexGlobal + j]
                LocValues[MaxDim*i + j] -= mu*(XNew[j]-(x + Xlam))
                #LocValues[MaxDim*i + j] -= mu*(XNew[j]-Xlam)
    elif mu < 0.0:
        pprint("Please use mu > 0.0 for backward step")
        exit(1)
    if Tangential == True:
        for i in range(0, dmax-dmin):
            value = 0.0
            for j in range(MaxDim):
                NX = N.vector()[MaxDim*(i+dmin) + j]
                MoveX = LocValues[MaxDim*i + j]
                value = value + NX*MoveX
            for j in range(MaxDim):
                NX = N.vector()[MaxDim*(i+dmin) + j]
                MoveX = LocValues[MaxDim*i + j]
                LocValues[MaxDim*i + j] = MoveX - value*NX
    MoveNew.vector().set_local(LocValues)
    MoveNew.vector().apply("")
    if MaxDim == 1:
        zero = 0.0
    if MaxDim == 2:
        zero = (0.0,0.0)
    if MaxDim == 3:
        zero = (0.0,0.0,0.0)
    for i in FixedBoundary:
        if Codim == 0:
            bc = DirichletBC(MoveSpaceV, zero, boundary_parts, i)
            bc.apply(MoveNew.vector())
    L1B = norm(MoveNew.vector(), 'linf')
    L2B = norm(MoveNew)
    return (L1B, L2B, MoveNew)
    
def MakeProcessorMap(mesh):
    MaxDim = mesh.topology().dim()
    MyRank = MPI.rank(mpi_comm_world())
    Map = MeshFunction("int", mesh, MaxDim)
    for c in cells(mesh):
        Map.array()[c.index()] = MyRank
    return Map

#Centroid Voronoi Tessellation (CVT)
def CVTSmooth(mesh, boundary_parts, Codim, Tangential = False, FixPlanes = [], VariableVertices = [], VariableMarker = [], SymmX = [], SymmY = [], SymmZ = []):
    MaxDim = mesh.topology().dim()
    Dim = mesh.topology().dim()-Codim
    MoveSpaceV = VectorFunctionSpace(mesh, "CG", 1, MaxDim)
    MoveNew = Function(MoveSpaceV)
    SizeGlobal = len(MoveNew.vector())
    GlobalCentroids = numpy.zeros(MaxDim*mesh.size_global(0))
    GlobalAreas = numpy.zeros(mesh.size_global(0))
    (dmin, dmax) = MoveNew.vector().local_range()
    dmin = int(dmin/MaxDim)
    dmax = int(dmax/MaxDim)
    if Tangential == True:
        #face normal
        N = VolumeNormal(mesh, FixPlanes)
    #interior
    if Codim == 0:
        for v1 in vertices(mesh):
            IndexGlobal = v1.global_index()
            for e in cells(v1):
                if e.is_ghost() == False:
                    Centroid = numpy.zeros(MaxDim)
                    for v2 in vertices(e):
                        for j in range(MaxDim):
                            Centroid[j] += (1.0/(Dim+1))*v2.x(j)
                    Area = e.volume()
                    GlobalAreas[IndexGlobal] += Area
                    for j in range(MaxDim):
                        GlobalCentroids[MaxDim*IndexGlobal + j] += Area*Centroid[j]
    #boundary
    elif Codim == 1:
        for v1 in vertices(mesh):
            IndexGlobal = v1.global_index()
            if VariableVertices[IndexGlobal] > 0:
                for e in facets(v1):
                    if e.exterior() == True and e.is_ghost()==False and boundary_parts[e.index()] in VariableMarker:
                        if MPI.size(mpi_comm_world()) == 1:
                            ID = e.index()
                        else:
                            ID = e.global_index()
                        Centroid = numpy.zeros(MaxDim)
                        for v2 in vertices(e):
                            for j in range(MaxDim):
                                Centroid[j] += (1.0/(Dim+1))*v2.x(j)
                        if MaxDim == 3:
                            Area = Face(mesh, e.index()).area()
                        else:
                            Area = Cell(mesh, e.index()).area()
                        GlobalAreas[IndexGlobal] += Area
                        for j in range(MaxDim):
                            GlobalCentroids[MaxDim*IndexGlobal + j] += Area*Centroid[j]
    SyncSum(GlobalCentroids)
    SyncSum(GlobalAreas)
    LocMove = numpy.zeros(MaxDim*(dmax-dmin))
    d2v = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
    for i in range(0, dmax-dmin):
        v1 = Vertex(mesh, d2v[i])
        if Codim == 0 or VariableVertices[v1.global_index()] > 0:
            IndexGlobal = v1.global_index()
            MyArea = GlobalAreas[IndexGlobal]
            if MyArea > 0.0:
                for j in range(MaxDim):
                    LocMove[MaxDim*i+j] = (1.0/MyArea)*GlobalCentroids[MaxDim*IndexGlobal + j]
                    LocMove[MaxDim*i+j] -= v1.x(j)
                if Tangential == True:
                    value = 0.0
                    for j in range(MaxDim):
                        value += LocMove[MaxDim*i+j]*N.vector()[MaxDim*(i)+j]
                    for j in range(MaxDim):
                        LocMove[MaxDim*i+j] -= value*N.vector()[MaxDim*(i)+j]
            elif MyArea == 0.0:
                dprint("Codim %d: AREA PROBLEM!"%Codim)
    MoveNew.vector().set_local(LocMove)
    MoveNew.vector().apply("")
    if MaxDim == 1:
        zero = 0.0
    if MaxDim == 2:
        zero = (0.0,0.0)
    if MaxDim == 3:
        zero = (0.0,0.0,0.0)
    #keep boundaries constant if repairing the interior
    if Codim == 0:
        if SymmX != [] or SymmY != [] or SymmZ != []:
            FixedMarker = list(set(range(1,numpy.max(boundary_parts.array()))) - set(SymmX + SymmY + SymmZ))
            bc = []
            for i in FixedMarker:
                bc.append(DirichletBC(MoveSpaceV, zero, boundary_parts, i))
            for i in SymmX:
                bc.append(DirichletBC(MoveSpaceV.sub(0), 0.0, boundary_parts, i))
            for i in SymmY:
                bc.append(DirichletBC(MoveSpaceV.sub(1), 0.0, boundary_parts, i))
            for i in SymmZ:
                bc.append(DirichletBC(MoveSpaceV.sub(2), 0.0, boundary_parts, i))
            for MyBC in bc:
                MyBC.apply(MoveNew.vector())
        else:
            bc = DirichletBC(MoveSpaceV, zero, "on_boundary")
            bc.apply(MoveNew.vector())
    #fix planes:
    for i in FixPlanes:
        def Plane(x, on_boundary):
            return abs(x[i]) < DOLFIN_EPS
        bc = DirichletBC(MoveSpaceV.sub(i), 0.0, Plane)
        bc.apply(MoveNew.vector())
    L1B = norm(MoveNew.vector(), 'linf')
    L2B = norm(MoveNew)
    return (L1B, L2B, MoveNew)

#same as curvature flow with implicit euler timestepping
def ContinuousMeshLaplaceSmooth(gamma, lam):
    V = VectorFunctionSpace(gamma, "CG", 1, 3)
    X = interpolate(Expression(("x[0]", "x[1]", "x[2]"), element=V.ufl_element()), V)
    X_next = TrialFunction(V)
    eta = TestFunction(V)
    a = inner(X_next, eta)*dx + lam*inner(grad(X_next), grad(eta))*dx
    l = inner(X,eta)*dx
    bc0 = DirichletBC(V, X, "on_boundary")
    X2 = Function(V)
    solve(a==l, X2, bc0)
    return project(X2-X,V)
    
def DiscreteMeshRepair(mesh, boundary_parts, SmoothVolume, SmoothBoundary, Tangential = True, MaxIter = 99999, Step = 1.0, Stop = 1e-6, FixPlanes = [], VariableBoundary = [[1,2,3,4,5,6,7,8,9,10]], SymmX = [], SymmY = [], SymmZ = [], Vis = False, InitField = [], Curvature = []):
    #Determine indices of fixes vertices
    #this will preserve edges between regions of different VariableBoundary values in boundary_parts
    OnVariable = []
    for i in range(len(VariableBoundary)):
        OnVariable.append(VertexOnBoundary(mesh, boundary_parts, VariableBoundary[i], True, SymmX+SymmY+SymmZ))
        
    CopyMesh = Mesh(mesh)
    CopyBP = MeshFunction("size_t", CopyMesh, CopyMesh.geometry().dim()-1)
    for i in range(len(boundary_parts.array())):
        CopyBP.array()[i] = boundary_parts.array()[i]
    if InitField != []:
        CopyMesh.move(InitField)
    Move = Function(VectorFunctionSpace(mesh, "CG", 1))
    DeltaMove = Function(VectorFunctionSpace(CopyMesh, "CG", 1))
    L1B = 0.0; L2B = 0.0
    if SmoothVolume == True:
        (L1Bv, L2Bv, DeltaMovev) = CVTSmooth(CopyMesh, CopyBP, 0, Tangential, FixPlanes, [], [])
        DeltaMove = AddVectors(DeltaMove, DeltaMovev, 1.0, Step)
        L1B = L1B + L1Bv
        L2B = L2B + L2Bv
    if SmoothBoundary == True:
        for j in range(len(VariableBoundary)):
            (L1Bb, L2Bb, DeltaMoveb) = CVTSmooth(CopyMesh, CopyBP, 1, Tangential, FixPlanes, OnVariable[j], VariableBoundary[j])
            DeltaMove = AddVectors(DeltaMove, DeltaMoveb, 1.0, Step)
            L1B = L1B + L1Bb
            L2B = L2B + L2Bb
    i = 0
    if Vis:
        pprint("Repair Visalization on")
        VisFile = File("tmp/ConvertDefo.pvd", "compressed")
        VisFile << (DeltaMove, float(i))
    pprint("Mesh Smooth: %4d, L1B = %e, L2B = %e"%(i, L1B, L2B))
    CopyMesh.move(DeltaMove)
    Move = AddVectors(Move, DeltaMove)
    while L1B > Stop and i < MaxIter:
        i+=1
        L1B = 0.0; L2B = 0.0
        DeltaMove = Function(VectorFunctionSpace(CopyMesh, "CG", 1))
        if SmoothVolume == True:
            (L1Bv, L2Bv, DeltaMovev) = CVTSmooth(CopyMesh, CopyBP, 0, Tangential, FixPlanes, [], [])
            DeltaMove = AddVectors(DeltaMove, DeltaMovev, 1.0, Step)
            L1B = L1B + L1Bv
            L2B = L2B + L2Bv
        if SmoothBoundary == True:
            for j in range(len(VariableBoundary)):
                (L1Bb, L2Bb, DeltaMoveb) = CVTSmooth(CopyMesh, CopyBP, 1, Tangential, FixPlanes, OnVariable[j], VariableBoundary[j])
                DeltaMove = AddVectors(DeltaMove, DeltaMoveb, 1.0, Step)
                L1B = L1B + L1Bb
                L2B = L2B + L2Bb
        pprint("Mesh Smooth: %4d, L1B = %e, L2B = %e"%(i, L1B, L2B))
        if Vis:
            VisFile << (DeltaMove, float(i))
        CopyMesh.move(DeltaMove)
        Move = AddVectors(Move, DeltaMove)
        Move.rename("Repair", "dummy")
    Move.rename("Repair", "dummy")
    return (L1B, L2B, Move)

def MyDiscreteMeshRepair(mesh, gamma, lam, tol, MaxIter=100):
    MaxDim = 3
    MoveSpaceS = FunctionSpace(gamma, "CG", 1)
    MoveSpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
    #return value
    Move = Function(MoveSpaceV)
    meshCopy = Mesh(mesh)
    meshCopy.init()
    meshCopy.order()
    gammaCopy = BoundaryMesh(meshCopy, "exterior")
    gammaCopy.init()
    gammaCopy.order()

    #face normal
    n = SurfaceFaceNormal(mesh)
    #point normal
    n = AverageFaceQuantity(n)
    NormalizeVectorField(n)
    DOFs = vertex_to_dof_map(MoveSpaceS)
    Offset = Function(MoveSpaceV)

    #turn all coordinates into a vector and calculate N (=patch size)
    GT= gamma.topology()
    NumVertex = GT.size_global(0)
    MyCoordinates = numpy.zeros([NumVertex, MaxDim])
    MyCopyCoordiantes = numpy.zeros([NumVertex, MaxDim])
    MyNeighbours = [[] for k in range(NumVertex)]
    for v1 in vertices(gamma):
        Index = v1.index()
        for j in range(MaxDim):
            MyCoordinates[Index, j] = v1.x(j)
            MyCopyCoordiantes[Index, j] = v1.x(j)
        for e in edges(v1):
            for j in range(2):
                if not e.entities(0)[j] == Index:
                    MyNeighbours[Index].append(e.entities(0)[j])

    #calculate initial badness:
    #Offset = numpy.zeros([NumVertex, MaxDim])
    x_c = numpy.zeros([NumVertex, MaxDim])
    MyInnerDerivative = numpy.zeros([NumVertex, MaxDim, MaxDim])
    for k in range(NumVertex):
        #compute average in tangent plane
        N = len(MyNeighbours[k])
        #calculate center in tangent plane
        x_k = MyCoordinates[k]
        n_k = numpy.empty(MaxDim)
        #calcuate tangent center
        for j in range(MaxDim):
            n_k[j] = n.vector()[MaxDim*int(DOFs[k]) + j]
        for i in range(N):
            x_i = MyCoordinates[MyNeighbours[k][i]]
            x_c[k] = x_c[k] + (1.0/N)*(x_i - numpy.dot(x_i-x_k,n_k)*n_k)
        #tangent center has been calculated
        for j in range(MaxDim):
            Offset.vector()[MaxDim*DOFs[k]+j] = x_k[j]-x_c[k][j]
        #compute gradient
        for j in range(MaxDim):
            e = numpy.zeros(MaxDim)
            e[j] = 1.0
            #create inner derivative of center node
            MyInnerDerivative[k][j] = MyInnerDerivative[k][j] + e - numpy.dot(e, n_k)*n_k
            #create inner derivative of halo nodes
            for i in range(N):
                MyInnerDerivative[MyNeighbours[k][i]][j] = MyInnerDerivative[MyNeighbours[k][i]][j] + (1.0/N)*(e - numpy.dot(e, n_k)*n_k)
        #inner derivative computed

    #remove fixed nodes from norm
    Offset = InjectOnSubSurface(Offset, mesh, VariableBoundary)
    Badness = 0.0
    for k in range(NumVertex):
        for j in range(MaxDim):
            Badness = Badness + 0.5*numpy.dot(Offset.vector()[MaxDim*k + j], Offset.vector()[MaxDim*k + j])
    #start projection loop
    count = 0
    print "Repair:", count, "Step:", lam, "L2-Badness:", Badness, "L1-Badness:", max(Offset.vector())
    while max(Offset.vector()) > tol and count < MaxIter:
        count = count + 1
        #compute gradient
        MyGradient = Function(MoveSpaceV)
        for k in range(NumVertex):
            x_k = MyCoordinates[k]
            OuterDerivative = numpy.zeros(MaxDim)
            for j in range(MaxDim):
                OuterDerivative[j] = Offset.vector()[MaxDim*DOFs[k] + j]
            for j in range(MaxDim):
                MyGradient.vector()[MaxDim*DOFs[k]+j] = numpy.dot(OuterDerivative,MyInnerDerivative[k])[j]

        #remove fixed nodes
        MyGradient = InjectOnSubSurface(MyGradient, mesh, VariableBoundary))

        #update coordinates / gradient descent
        for k in range(NumVertex):
            for j in range(MaxDim):
                MyCoordinates[k][j] = MyCoordinates[k][j] - lam*MyGradient.vector()[MaxDim*DOFs[k]+j]
            
        #caluclate new badness and derivatives
        x_c = numpy.zeros([NumVertex, MaxDim])
        MyInnerDerivative = numpy.zeros([NumVertex, MaxDim, MaxDim])
        #interate over all center nodes
        for k in range(NumVertex):
            #compute average in tangent plane
            N = len(MyNeighbours[k])
            #calculate center in tangent plane
            x_k = MyCoordinates[k]
            n_k = numpy.empty(MaxDim)
            #calcuate tangent center
            for j in range(MaxDim):
                n_k[j] = n.vector()[MaxDim*int(DOFs[k]) + j]
            for i in range(N):
                x_i = MyCoordinates[MyNeighbours[k][i]]
                x_c[k] = x_c[k] + (1.0/N)*(x_i - numpy.dot(x_i-x_k,n_k)*n_k)
            #tangent center has been calculated
            for j in range(MaxDim):
                Offset.vector()[MaxDim*DOFs[k]+j] = x_k[j]-x_c[k][j]
            #compute gradient
            for j in range(MaxDim):
                e = numpy.zeros(MaxDim)
                e[j] = 1.0
                #create inner derivative of center node
                MyInnerDerivative[k][j] = MyInnerDerivative[k][j] + e - numpy.dot(e, n_k)*n_k
                #create inner derivative of halo nodes
                for i in range(N):
                    MyInnerDerivative[MyNeighbours[k][i]][j] = MyInnerDerivative[MyNeighbours[k][i]][j] + (1.0/N)*(e - numpy.dot(e, n_k)*n_k)
            #inner derivative computed
        #end loop all vertices

        #remove fixed nodes from norm
        Offset = InjectOnSubSurface(Offset, mesh, VariableBoundary)
        #bc.apply(Offset.vector())
        BadnessNew = 0.0
        for k in range(NumVertex):
            for j in range(MaxDim):
                BadnessNew = BadnessNew + 0.5*numpy.dot(Offset.vector()[MaxDim*k + j], Offset.vector()[MaxDim*k + j])
        if BadnessNew > Badness:
            print "MESH REPAIR:", count, "REDUCING STEP"
            print "BadnessOld:", Badness, "BadnessNew", BadnessNew
            lam = 0.5*lam
        else:
            Badness = BadnessNew
            print "Repair:", count, "Step:", lam, "L2-Badness:", Badness, "L1-Badness:", max(Offset.vector())
            #calculate new normals
            #easiest by using gammaCopy...
            for v1 in vertices(gammaCopy):
                Index = v1.index()
                for j in range(MaxDim):
                    gammaCopy.coordinates()[Index][j] = MyCoordinates[Index, j]
            gammaCopy.init()

            #update normals
            #print "Step Accepted"
            #make new point normal
            n = SurfaceFaceNormal(meshCopy)
            n = AverageFaceQuantity(n)
            NormalizeVectorField(n)
    #while loop terminated
    #mesh repaired
    for k in range(NumVertex):
        for j in range(MaxDim):
            Move.vector()[MaxDim*DOFs[k]+j] = MyCoordinates[k, j] - MyCopyCoordiantes[k, j]
    return (Badness, max(Offset.vector()), Move)
