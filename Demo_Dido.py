from dolfin import *
from SAD_geometry import VolumeNormal, ComputeDivNVolume

from ShapeOpt import *

set_log_active(False)
set_log_level(PROGRESS)

mesh = UnitSquareMesh(10, 10)

FS = FiniteElement("CG", mesh.ufl_cell(), 1)
FV = VectorElement("CG", mesh.ufl_cell(), 1)
FR = FiniteElement("R", mesh.ufl_cell(), 0)
TotalSpace = FunctionSpace(mesh, FV*FR)

Q0 = Function(TotalSpace)
(V0, lmb0) = Q0.split()
lmb0.rename("lmb0", "")
(V, lmb) = TestFunctions(TotalSpace)
(W, dlmb) = TrialFunctions(TotalSpace)

#Declare Lagrangian
OneV = Constant(1.0)
OneV.rename("One", "")
OneR = Expression("1.0", element=FR, domain=mesh)
Vol0 = assemble(OneR*dx)
print("Initial Volume: %e"%Vol0)

L = OneV*ds(domain=mesh) + lmb0*(OneV*dx(domain=mesh))

N = VolumeNormal(mesh)
N.rename("n", "")

MyMode = Strong
#Volume gradient is also possible, but then remeshing is necessary...
#MyMode = WeakMaterial

if MyMode == Strong:
    StepLength= Constant(1.0) #for strong form
    StopIter = 300
#for demonstration purposes of weak form. Actual optimization needs remeshing
else:
    StepLength= Constant(0.1)
    StopIter = 30

MeshSmooth = Constant(0.1)

CurvSmooth = 1e-2
kappa = ComputeDivNVolume(mesh, CurvSmooth)

print("Creating shape derivative...")
sd = ShapeDerivative(L, mesh, V, n=N, State=lmb0, StateDirection=lmb, kappa=kappa, Is_Normal=[V], Constant_In_Normal=[], Mode=MyMode)
print("Shape derivative is:")
print("%s"%sd)


print("Creating shape Hessian...")
sh = ShapeDerivative(sd, mesh, W, n=N, State=lmb0, StateDirection=dlmb, kappa=kappa, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, Mode=MyMode)
print("Shape Hessian is:")
print("%s"%sh)
#exit()


g = Function(TotalSpace)

(V1, U1) = TrialFunctions(TotalSpace)
(V2, U2) = TestFunctions(TotalSpace)

Optimality = 1e9

FOut = File("output/SAD_Dido/Move.pvd")
FGrad = File("output/SAD_Dido/Gradient.pvd")

#convergence history
if MPI.rank(mpi_comm_world()) == 0:
    fHistory = open("output/SAD_Dido/"+"History.dat", "w")
    fHistory.write("Iter\tObjective\tOptimality\n")

iter = 0
while Optimality > 2e-3 and iter < StopIter:
    #print OptIter, "SOLVING KKT"
    CurVol = assemble(OneR*dx)
    #update normal and curvature:
    N.assign(VolumeNormal(mesh))
    kappa.assign(ComputeDivNVolume(mesh, CurvSmooth))

    #First create mesh deformation for mesh nodes
    KKT = inner(V1,V2)*dx + MeshSmooth*inner(grad(V1),grad(V2))*dx
    KKT += inner(V1,V2)*ds + MeshSmooth*inner(grad(V1),grad(V2))*ds
    #add Hessian
    KKT += sh
    
    (A,b) = assemble_system(KKT, -sd)

    #Remove interior residual from optimality check and gradient
    #Not strictly necessary but helps if volume formulation is used
    if not MyMode == Strong:
        SurfGrad = Function(TotalSpace)
        SurfGrad.rename("Gradient", "")
        Tmp = Function(TotalSpace)
        Tmp.vector()[:] = b[:]
        KillInterior = DirichletBC(TotalSpace, Tmp, "on_boundary")
        KillInterior.apply(SurfGrad.vector())
        b = SurfGrad.vector()
        FGrad << SurfGrad

    #print the Hessian?
    #print A.array()
    b[len(b)-1] = Vol0-CurVol
    solve(A, g.vector(), b)
    #solve(KKT == -sd, g)
    (dDefo, dLambda) = g.split()
    dDefo.rename("Deformation", "")

    FOut << dDefo
    plot(dDefo)#, interactive=False)
    #update state and adjoint:
    lmb0.assign(project(lmb0+dLambda, FunctionSpace(mesh, FR)))

    Optimality = norm(b)
    obj = assemble(OneV*ds(domain=mesh))
    print("Iter %4d Optimality: %e Length: %e Volume-offet: %e"%(iter,Optimality,norm(g),Vol0-CurVol))
    if MPI.rank(mpi_comm_world()) == 0:
        fHistory.write("%4d\t%e\t%e\n"%(iter, obj, Optimality))
    
    #move = project(StepLength*dDefo, FV)
    MyMove = project(StepLength*dDefo, FunctionSpace(mesh, FV))
    ALE.move(mesh, MyMove)
    iter = iter + 1
