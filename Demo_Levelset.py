from dolfin import *
from ShapeOpt import *
from SAD_geometry import VolumeNormal, ComputeDivNVolume

set_log_active(False)
set_log_level(PROGRESS)

mesh = UnitSquareMesh(10, 10)

FS = FunctionSpace(mesh, "CG", 1)
FV = VectorFunctionSpace(mesh, "CG", 1)
V = TestFunction(FV)
W = TrialFunction(FV)
N = VolumeNormal(mesh)
N.rename("n", "")
CurvSmooth = 1e-2
kappa = ComputeDivNVolume(mesh, CurvSmooth)

#Declare objective function:
#This describes an ellipsis
f = Expression("(x[0]-0.5)*(x[0]-0.5) + 2.0*(x[1]-0.5)*(x[1]-0.5) - 1.0", element=FS.ufl_element(), domain=mesh)
LevelSet = project(f, FS)
L = LevelSet*dx

sd = ShapeDerivative(L, mesh, V, N, kappa=kappa, Is_Normal=[V])
#sh = inner(V,W)*ds
#StepLength = Constant(0.05)
sh = ShapeDerivative(sd, mesh, W, N, kappa=kappa, Is_Normal=[V,W], SymmetryDirection=V)
StepLength = Constant(1.0)

#Define mesh deformation

MeshDefo = inner(V,W)*dx + inner(grad(V), grad(W))*dx + sh

g = Function(FV)
g.rename("Deformation", "")

solve(MeshDefo == -StepLength*sd, g)

F = File("output/SAD_Levelset/morph.pvd")
F << g
Optimality = norm(g)
while Optimality > 1e-6:
    ALE.move(mesh, g)
    LevelSet.assign(project(f, FS))
    N.assign(VolumeNormal(mesh))
    kappa.assign(ComputeDivNVolume(mesh, CurvSmooth))
    solve(MeshDefo == -StepLength*sd, g)
    F << g
    Optimality = norm(g)
    print("Optimality: %e"%Optimality)
#plot(mesh, interactive=True)
